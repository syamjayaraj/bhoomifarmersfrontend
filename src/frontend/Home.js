import Footer from "./Components/Footer/Footer";
import Header from "./Components/Header/Header";
import ProductList from "./Components/ProductList/ProductList";
import Slider from "./Components/Slider/Slider";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import Cart from "./Components/Cart/Cart";
import { getBestSellerProductsFrontend } from "../store/actions";

function Home() {
  const dispatch = useDispatch();

  const userData = useSelector((state) => state.UserFrontend.userData);
  const [deliverableActive, setDeliverableActive] = useState(false);
  const [allProActive, setAllProActive] = useState(false);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  useEffect(() => {
    if (userData.customer_id) {
      deliverableHandler();
    } else {
      allProductHandler();
    }
  }, [userData, orderWindow]);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);

  const deliverableHandler = () => {
    if (deliverableActive === false) {
      setDeliverableActive(true);
      setAllProActive(false);
    }
    dispatch(
      getBestSellerProductsFrontend({
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_type: 0,
      })
    );
  };

  const allProductHandler = () => {
    if (allProActive === false) {
      setDeliverableActive(false);
      setAllProActive(true);
    }

    dispatch(
      getBestSellerProductsFrontend({
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_type: 1,
      })
    );
  };

  return (
    <div>
      <Header />
      <Cart />
      <Slider title="Best Sellers" />

      <ProductList
        deliverableHandler={deliverableHandler}
        allProductHandler={allProductHandler}
        setDeliverableActive={setDeliverableActive}
        setAllProActive={setAllProActive}
        deliverableActive={deliverableActive}
        allProActive={allProActive}
      />
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default Home;
