import React, { useState, useEffect } from "react";
import OtpInput from "react-otp-input";
import { useDispatch, useSelector } from "react-redux";
import { setHideLoginModal, setUserDataFrontend } from "../../../store/actions";
import { Modal } from "react-bootstrap";

export default function LoginModal(props) {
  const [modalState, setModalState] = useState("login");
  const [mobileNumber, setMobileNumber] = useState("");
  const [otp, setOtp] = useState("");
  const [customer, setCustomer] = useState({});
  const [secondCounter, setSecondCounter] = useState(60);
  const [showCommunityList, setShowCommunityList] = useState(false);
  const [communitySelected, setCommunitySelected] = useState({});
  const [isActive, setIsActive] = useState(false);

  const [error, setError] = useState({
    message: "",
  });

  const communities = useSelector(
    (state) => state.CommunityFrontend.communities
  );

  const dispatch = useDispatch();

  useEffect(() => {
    setCommunitySelected(communities[0]);
  }, [communities]);

  let backToLogin = () => {
    setOtp("");
    setModalState("login");
  };

  let handleLoginModal = () => {
    dispatch(setHideLoginModal());
  };

  let login = () => {
    if (mobileNumber.length === 10) {
      setError({
        message: "",
      });
      setModalState("otp");
      setSecondCounter(60);
      setIsActive(true);
      fetch("https://api-bhoomi.mykar.in/api/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phone: mobileNumber,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            setModalState("otp");
          } else {
          }
        });
    } else {
      setError({
        message: "Enter a valid mobile number",
      });
    }
  };

  let verifyOtp = () => {
    if (otp.length === 4) {
      fetch("https://api-bhoomi.mykar.in/api/verifyOtp", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phone: mobileNumber,
          otp: otp,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            setError({
              message: "",
            });
            dispatch(setUserDataFrontend(result));
            if (result.customer_status === 1) {
              handleLoginModal();
            } else {
              setModalState("address");
            }
          } else {
            setError({
              message: "Incorrect OTP",
            });
          }
        });
    } else {
      setError({
        message: "Enter a valid OTP",
      });
    }
  };

  let register = () => {
    fetch("https://api-bhoomi.mykar.in/api/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        customer_name: customer.name,
        customer_mob: mobileNumber,
        customer_email: customer.email,
        customer_community_id: communitySelected.community_id,
        customer_apartment_no: customer.street,
        customer_message: customer.message,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.success) {
          dispatch(setUserDataFrontend(result));
          handleLoginModal();
          // dispatch(setCommunityGlobally(item));
          // dispatch(setCommunityGlobally(communitySelectedInLocalStorage));
          // dispatch(getOrderWindow(communitySelected.community_id));
        }
      });
  };

  let handleChangeCustomer = (e) => {
    setCustomer({
      ...customer,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    let interval = null;
    if (isActive) {
      if (secondCounter > 0)
        interval = setInterval(() => {
          setSecondCounter((secondCounter) => secondCounter - 1);
        }, 1000);
    } else if (!isActive && secondCounter !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isActive, secondCounter]);

  let selectCommunity = (comm) => {
    setCommunitySelected(comm);
    setShowCommunityList(false);
  };

  return (
    <Modal
      show={true}
      onHide={() => handleLoginModal()}
      className={`modal fade show ${
        modalState === "login"
          ? "login"
          : modalState === "otp"
          ? "otp"
          : modalState === "address"
          ? "addAddress"
          : null
      }`}
    >
      <Modal.Body>
        {modalState === "login" ? (
          <div>
            <button
              type="button"
              class="close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => handleLoginModal()}
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <h4 class="modal-title text-center mt-5" id="loginLabel">
              Login
            </h4>

            <div class="form-row">
              <div class="col-12 input-wrapper hasicon">
                <i class="icon fa fa-phone-alt"></i>
                <input
                  class="intxt"
                  type="number"
                  required
                  onChange={(e) => setMobileNumber(e.target.value)}
                  value={mobileNumber}
                />
                <label class="inplbl" for="user">
                  Enter your mobile no
                </label>
              </div>
              <div className="error-message">
                {error.message ? error.message : null}
              </div>
            </div>

            <div class="form-row mt-3">
              <div class="col-12 modbtncol">
                <button
                  id="loginNext"
                  data-bs-toggle="modal"
                  data-bs-target="#otpModal"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  class="btn btnsave w-100"
                  onClick={() => login()}
                >
                  Continue
                </button>
              </div>
            </div>
          </div>
        ) : modalState === "address" ? (
          <div>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <h4 class="modal-title text-center mt-5" id="yourAddressLabel">
              Your Address
            </h4>

            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  class="intxt"
                  type="text"
                  value="Vijay Shanthi"
                  required
                  name="name"
                  onChange={handleChangeCustomer}
                  value={customer.name}
                />
                <label class="inplbl" for="user">
                  Your Name
                </label>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  class="intxt"
                  type="email"
                  value="vjshanti@gmail.com"
                  required
                  name="email"
                  onChange={handleChangeCustomer}
                  value={customer.email}
                />
                <label class="inplbl" for="user">
                  Your Email address
                </label>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 input-wrapper">
                <div class="input-group-btn selectpanel" data-search="students">
                  <button
                    type="button"
                    class="btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    onClick={() => setShowCommunityList(!showCommunityList)}
                  >
                    <span class="search_by">
                      {communitySelected.community_name}
                    </span>
                  </button>
                  {showCommunityList ? (
                    <ul class="dropdown-menu" role="menu">
                      {communities.map((comm) => {
                        return (
                          <li onClick={() => selectCommunity(comm)}>
                            <a data-search="">{comm.community_name}</a>
                          </li>
                        );
                      })}
                    </ul>
                  ) : null}
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  class="intxt"
                  type="text"
                  required
                  name="street"
                  onChange={handleChangeCustomer}
                  value={customer.street}
                />
                <label class="inplbl" for="user">
                  Apartment Number
                </label>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 input-wrapper">
                <textarea
                  class="intxt"
                  type="number"
                  required
                  name="message"
                  onChange={handleChangeCustomer}
                  value={customer.message}
                ></textarea>
                <label class="inplbl" for="user">
                  Enter delivery Instruction
                </label>
              </div>
            </div>

            <div class="form-row mt-3">
              <div class="col-12 modbtncol">
                <button
                  id="yourAddressNext"
                  class="btn btnsave w-100"
                  onClick={register}
                >
                  Continue
                </button>
              </div>
            </div>
          </div>
        ) : modalState === "otp" ? (
          <div>
            <button
              type="button"
              class="close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => handleLoginModal()}
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <h4 class="modal-title text-center mt-5" id="otpLabel">
              OTP
            </h4>

            <div class="form-row">
              <div class="col-12 text-center">
                <p class="resend">
                  Please enter the OTP sent to mobile no
                  <br />
                  +91-{mobileNumber}{" "}
                  <button class="change" onClick={() => backToLogin()}>
                    Change
                  </button>
                </p>
              </div>
            </div>
            <div class="form-row">
              <OtpInput
                shouldAutoFocus
                value={otp}
                onChange={(value) => setOtp(value)}
                numInputs={4}
                separator={<span></span>}
                containerStyle={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  marginBottom: "1rem",
                  marginTop: "1rem",
                }}
                inputStyle={{
                  width: "48px",
                  height: "48px",
                  border: "1px solid #ddd",
                  lineHeight: "50px",
                  textAlign: "center",
                  fontSize: "24px",
                  color: "#333",
                  borderRadius: ".5rem",
                  margin: "0 6px",
                  outline: "0",
                  boxShadow: "none",
                }}
              />
            </div>
            <div class="form-row">
              <div class="col-12 text-center">
                <p class="resend">
                  <button
                    className="resend-button"
                    disabled={secondCounter > 0}
                    onClick={login}
                  >
                    Resend OTP{" "}
                    <span
                      style={{ display: secondCounter > 0 ? "inline" : "none" }}
                    >
                      in 00:{secondCounter}
                    </span>
                  </button>
                </p>
                <div className="error-message">
                  {error.message ? error.message : null}
                </div>
              </div>
            </div>

            <div class="form-row mt-3">
              <div class="col-12 modbtncol">
                <button
                  id="otpNext"
                  class="btn btnsave w-100"
                  onClick={() => verifyOtp()}
                >
                  Continue
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </Modal.Body>
    </Modal>
  );
}
