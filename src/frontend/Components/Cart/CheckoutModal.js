import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import moment from "moment";

function CheckoutModal(props) {
  let { setShowCheckoutModal } = props;

  let routeToHomePage = () => {
    setShowCheckoutModal(false);
    props.history.push("/");
  };

  console.log("inside modal");

  return (
    <Modal
      show={true}
      onHide={() => setShowCheckoutModal(false)}
      className="modal fade community show"
    >
      <Modal.Body>
        <button
          type="button"
          class="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={() => setShowCheckoutModal(false)}
        >
          <img src="/img/icons/close.svg" alt />
        </button>
        <img src="/img/logo/logo.svg" class="bhlogo mt-5" alt />

        <img src="/img/icons/success.svg" class="mx-auto d-block mt-5" alt />
        <h4 class="modal-title text-center mt-2" id="successLabel">
          Success!
        </h4>
        <div class="form-row">
          <div class="col-12 text-center">
            <p>
              Your order <b>#{props.orderResponse.order_number}</b> has been
              placed successfully. We will deliver your products on{" "}
              <b>
                {moment(props.orderResponse.delivery_date).format(
                  "ddd, DD MMM"
                )}
              </b>
            </p>
          </div>
        </div>

        <div class="form-row">
          <div class="col-12 text-center">
            <p>We will notify the order status through email. Stay tuned.</p>
          </div>
        </div>

        <div class="form-row mt-3">
          <div class="col-12 modbtncol">
            <button
              id="otpNext"
              class="btn btnsave w-100"
              onClick={() => routeToHomePage()}
            >
              Go to Homepage
            </button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}
export default withRouter(CheckoutModal);
