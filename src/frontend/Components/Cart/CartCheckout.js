import React, { useState, useEffect } from "react";
import {
  getBillAmount,
  getGSTAmount,
  getTotalAmount,
} from "../../Helper/CartCheckoutHelper";
import { useDispatch, useSelector } from "react-redux";
import {
  addToCart,
  clearCart,
  getCart,
  setHideCart,
  setShowLoginModal,
} from "../../../store/actions";
import axios from "axios";
import moment from "moment";

let logo = "/img/logo/logo.svg";

function CartCheckout(props) {
  const allProducts = useSelector((state) => state.ProductFrontend.allProducts);
  const cartGlobal = useSelector((state) => state.CartFrontend.cart);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  const userData = useSelector((state) => state.UserFrontend.userData);
  const [cart, setCart] = useState([]);
  const [canOrder, setCanOrder] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    let startDayNumber = orderWindow.order_window_start_day_no;
    let endDayNumber = orderWindow.order_window_end_day_no;

    let daysAndNumbers = {
      Sun: 7,
      Mon: 1,
      Tue: 2,
      Wed: 3,
      Thu: 4,
      Fri: 5,
      Sat: 6,
    };
    let dateNumberToday = daysAndNumbers[moment().format("ddd")];

    if (startDayNumber <= dateNumberToday && dateNumberToday <= endDayNumber) {
      let startTime = orderWindow.order_window_start_time;
      let endTime = orderWindow.order_window_end_time;
      let timeNow = moment().format("HH:mm:ss");
      if (startTime <= timeNow && timeNow <= endTime) {
        setCanOrder(true);
      }
    } else {
      setCanOrder(false);
    }
  }, [orderWindow]);

  useEffect(() => {
    setCart(cartGlobal);
  }, [cartGlobal]);

  let checkout = () => {
    if (userData.customer_id) {
      displayRazorpay();
    } else {
      dispatch(setShowLoginModal());
    }
  };

  function loadScript(src) {
    return new Promise((resolve) => {
      const script = document.createElement("script");
      script.src = src;
      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };
      document.body.appendChild(script);
    });
  }

  async function displayRazorpay() {
    const res = await loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );
    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }
    // creating a new order
    fetch("https://api-bhoomi.mykar.in/api/checkout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        customer_id: userData.customer_id,
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (!result) {
          alert("Server error. Are you online?");
          return;
        }
        if (result.success) {
          console.log(result, "kll");
          props.setOrderResponse(result);
          // Getting the order details back
          const { order_amount, o_razorpay_order_id, order_id } = result;
          const options = {
            key: "rzp_test_ewExPqccvu7cFJ", // Enter the Key ID generated from the Dashboard
            amount: (order_amount * 100).toString(),
            currency: "INR",
            name: "Bhoomi Farmers",
            description: "Test Transaction",
            image: { logo },
            order_id: o_razorpay_order_id,
            handler: async function (response) {
              const data = {
                order_id: order_id,
                payment_id: response.razorpay_payment_id,
                o_razorpay_order_id: response.razorpay_order_id,
                razorpay_ignature: response.razorpay_signature,
              };
              console.log(data, "ddd");
              fetch("https://api-bhoomi.mykar.in/api/checkoutsuccess", {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
              })
                .then((successRes) => successRes.json())
                .then((successRes) => {
                  props.setShowCheckoutModal(true);
                  dispatch(clearCart());
                  dispatch(setHideCart());
                });
            },
            prefill: {
              name: userData.customer_name,
              email: userData.customer_email,
              contact: userData.customer_mob,
            },
            notes: {
              address: "",
            },
            theme: {
              color: "#61dafb",
            },
          };

          const paymentObject = new window.Razorpay(options);
          paymentObject.open();
        }
      });
  }

  return (
    <div class="cartchekout">
      <div class="summary">
        <h5>Checkout Summary</h5>
        <span class="amrow">
          Bill Amount{" "}
          <span class="amount">Rs. {getBillAmount(cart, allProducts)}</span>
        </span>

        <span class="amrow">
          Delivery Charges <span class="amount">Free</span>
        </span>
        <span class="amrow total">
          Total Order Amount{" "}
          <span class="amount">Rs. {getTotalAmount(cart, allProducts)}</span>
        </span>
        <button
          class="checkout"
          onClick={checkout}
          disabled={cart.length == 0 || !canOrder}
          style={
            cart.length == 0 || !canOrder
              ? { backgroundColor: "rgb(151, 151, 151)" }
              : null
          }
        >
          Proceed to Checkout
        </button>
        {!canOrder ? (
          <div
            style={{
              textAlign: "center",
              color: "#f35555",
              marginTop: "1rem",
            }}
          >
            You can not place order at this time
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default CartCheckout;
