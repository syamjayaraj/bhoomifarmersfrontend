import react from "react";
import { setHideCart } from "../../../store/actions";
import { useDispatch, useSelector } from "react-redux";

export default function CartEmpty() {
  const dispatch = useDispatch();

  return (
    <div class="container-fluid mt-100">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body cart">
              <div class="col-sm-12 empty-cart-cls text-center">
                <img
                  src="/img/icons/cart.svg"
                  width="60"
                  height="60"
                  class="img-fluid mb-4 mr-3"
                />
                <h3>
                  <strong>Your Cart is Empty</strong>
                </h3>
                <h4>Add something to checkout</h4>{" "}
                <a
                  onClick={() => dispatch(setHideCart())}
                  class="btn btn-warning cart-btn-transform m-3"
                  data-abc="true"
                  style={{
                    color: "white",
                  }}
                >
                  continue shopping
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
