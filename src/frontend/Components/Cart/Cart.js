import React, { useState } from "react";
import CartCheckout from "./CartCheckout";
import CartHead from "./CartHead";
import CartItemList from "./CartItemList";
import CartEmpty from "./CartEmpty";
import CheckoutModal from "./CheckoutModal";
import { useSelector } from "react-redux";

function Cart() {
  const { showCart } = useSelector((state) => state.UiFrontend);
  const cartGlobal = useSelector((state) => state.CartFrontend.cart);
  const [showCheckoutModal, setShowCheckoutModal] = useState(false);
  const [orderResponse, setOrderResponse] = useState({});

  return (
    <div class={showCart ? "quickcart show" : "quickcart"}>
      {showCheckoutModal ? (
        <CheckoutModal
          setShowCheckoutModal={setShowCheckoutModal}
          orderResponse={orderResponse}
        />
      ) : null}
      <CartHead />
      {cartGlobal.length === 0 ? (
        <CartEmpty />
      ) : (
        <div class="cartcont">
          <CartItemList />
          <CartCheckout
            setShowCheckoutModal={setShowCheckoutModal}
            setOrderResponse={setOrderResponse}
          />
        </div>
      )}
    </div>
  );
}

export default Cart;
