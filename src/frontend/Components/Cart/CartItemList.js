import React, { useEffect, useState } from "react";
import CartItem from "./CartItem";

// Redux Import
import { useSelector } from "react-redux";

function CartItemList() {
  const [cart, setCart] = useState([]);

  const cartGlobal = useSelector((state) => state.CartFrontend.cart);

  useEffect(() => {
    setCart(cartGlobal);
  }, [cartGlobal, allProducts]);

  const allProducts = useSelector((state) => state.ProductFrontend.allProducts);

  console.log(allProducts, "allp");

  return (
    <div class="cartitems custscrollA">
      <ul class="cartlist">
        {cart &&
          cart.map((item) => {
            return (
              allProducts &&
              allProducts.map((product) => {
                if (product.product_id === item.cart_product_id) {
                  return <CartItem item={item} product={product} />;
                }
              })
            );
          })}
      </ul>
    </div>
  );
}

export default CartItemList;
