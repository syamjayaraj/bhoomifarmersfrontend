import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { updateCartQty, deleteFromCart } from "../../../store/actions";
import { imageLink } from "../../Helper/ImageLink";

function CartItem(props) {
  const dispatch = useDispatch();
  const [inputEmpty, setInputEmpty] = useState(false);
  const item = props.item;
  const product = props.product;
  const incqtyhandler = () => {
    let cartqty = item.cart_quantity;
    console.log(cartqty, product.product_moq, "indi");
    if (cartqty < product.product_moq) {
      cartqty = item.cart_quantity + 1;
      dispatch(updateCartQty({ id: item.cart_id, new_quantity: cartqty }));
    }
  };
  const decqtyhandler = () => {
    var cartqty = item.cart_quantity - 1;

    if (cartqty > 0) {
      dispatch(updateCartQty({ id: item.cart_id, new_quantity: cartqty }));
    }
  };

  let handleChangeItemQty = (e) => {
    let value = e.target.value;
    if (!e.target.value) {
      setInputEmpty(true);
    } else {
      dispatch(updateCartQty({ id: item.cart_id, new_quantity: value }));
      setInputEmpty(false);
    }
  };

  return (
    <li>
      <span class="cartpro">
        <span class="cartimg">
          <a href="#">
            <img src={`${imageLink()}${product.product_image_main}`} alt="" />
          </a>
        </span>
        <span class="cartdet">
          <span
            class="remove"
            title="Remove"
            onClick={() => {
              dispatch(deleteFromCart(item.cart_id));
            }}
          >
            <span class="fa fa-trash-alt"></span>
          </span>
          <a href="#" class="name">
            {product.product_name_english}
          </a>
          <form class="cartform">
            <input
              type="button"
              class="qtyminus"
              field="quantity"
              onClick={decqtyhandler}
            />
            <input
              type="text"
              name="quantity"
              value={inputEmpty ? "" : item.cart_quantity}
              class="qty"
              onChange={handleChangeItemQty}
            />
            <input
              type="button"
              class="qtyplus"
              field="quantity"
              onClick={incqtyhandler}
            />
          </form>
          <span class="price">Rs. {product.product_sp}</span>
        </span>
      </span>
    </li>
  );
}

export default CartItem;
