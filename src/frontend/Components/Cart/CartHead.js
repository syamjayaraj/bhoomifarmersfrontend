import React from "react";
import { setHideCart } from "../../../store/actions";
import { useDispatch, useSelector } from "react-redux";

function CartHead() {
  const dispatch = useDispatch();

  return (
    <div class="carthead">
      <h3>Cart</h3>
      <button
        class="closebtn"
        title="Close"
        onClick={() => dispatch(setHideCart())}
      >
        <i class="fa fa-times"></i>
      </button>
    </div>
  );
}

export default CartHead;
