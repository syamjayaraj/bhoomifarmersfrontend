import React, { useEffect, useState } from "react";

import Probox from "../Probox/Probox";

import { useSelector } from "react-redux";
import { withRouter } from "react-router";

function ProductList(props) {
  let {
    deliverableActive,
    allProActive,
    deliverableHandler,
    allProductHandler,
    type,
  } = props;
  const cart = useSelector((state) => state.CartFrontend.cart);
  const products = useSelector((state) => state.ProductFrontend.products);

  return (
    <div className="container-fluid cmpad">
      <div
        className="row"
        style={{
          marginTop: type === "category" ? "3rem" : 0,
        }}
      >
        <div className="col-sm-6">
          <h1 className="proboxlist__heading">
            {props.title ? props.title : "Best Sellers"}
          </h1>
        </div>
        <div className="col-sm-6 d-none d-sm-block">
          <ul className="show">
            <li>Show: </li>
            <li>
              <button
                className={
                  deliverableActive
                    ? "deliverable showbtn active"
                    : "deliverable showbtn"
                }
                onClick={deliverableHandler}
              >
                Deliverable
              </button>
            </li>
            <li>
              <button
                className={
                  allProActive ? "allpro showbtn active" : "allpro showbtn"
                }
                onClick={allProductHandler}
              >
                All Products
              </button>
            </li>
          </ul>
        </div>
      </div>
      <div className="row prodlist">
        {products &&
          products.map((product) => {
            let item1 = null;

            cart &&
              cart.map((item) => {
                if (product.product_id === item.cart_product_id) {
                  item1 = item;
                }
              });
            return (
              <Probox
                product={product}
                key={product.product_id}
                permalink={product.product_permalink}
                cart={item1}
                id={product.product_id}
                image={product.product_image_main}
                name={product.product_name_english}
                price={product.product_sp}
                stock_status={product.stock_status}
                deliverable={deliverableActive ? true : false}
              />
            );
          })}
      </div>
    </div>
  );
}

export default withRouter(ProductList);
