import React, { useEffect, useState } from "react";

// Swiper Imports
import "swiper/swiper-bundle.css";
import "swiper/swiper-bundle.min.css";
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
// Redux
import { useDispatch, useSelector } from "react-redux";
import Probox from "../../Probox/Probox";

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);
function TopSellers(props) {
  const userData = useSelector((state) => state.UserFrontend.userData);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  let { product } = props;
  const [products, setProducts] = useState([]);
  const cart = useSelector((state) => state.CartFrontend.cart);
  useEffect(() => {
    fetchTopSellers();
  }, []);

  let fetchTopSellers = () => {
    fetch("https://api-bhoomi.mykar.in/api/category_wise_top_seller_list", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        category_id: product[0].product_category,
        product_type: 0,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.success) {
          setProducts(result.data);
        }
      });
  };

  return (
    <div className="productlist w-100 pb-0">
      <h2 className="mainhead">Top Sellers</h2>

      <div className="row">
        <div className="container-fluid slidercon">
          <Swiper
            spaceBetween={10}
            slidesPerView={1}
            slidesPerColumn={2}
            navigation={{
              nextEl: ".pro-next",
              prevEl: ".pro-prev",
            }}
            pagination={{
              el: ".proslide-pagination",
              type: "bullets",
            }}
            breakpoints={{
              640: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              768: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
              1024: {
                slidesPerView: 3,
                spaceBetween: 10,
              },
            }}
            onSwiper={(swiper) => console.log(swiper)}
            onSlideChange={() => console.log("slide change")}
            slidesPerColumnFill="row"
            className="proslide"
          >
            {products.map((product) => {
              let item1 = null;

              cart.map((item) => {
                if (product.product_id === item.cart_product_id) {
                  item1 = item;
                }
              });
              return (
                <SwiperSlide>
                  <Probox
                    product={product}
                    key={product.product_id}
                    cart={item1}
                    id={product.product_id}
                    image={product.product_image_main}
                    name={product.product_name_english}
                    price={product.product_sp}
                  />
                </SwiperSlide>
              );
            })}

            <div className="swiper-pagination proslide-pagination"></div>
          </Swiper>

          <div className="swiper-button-next pro-next"></div>
          <div className="swiper-button-prev pro-prev"></div>
        </div>
      </div>
    </div>
  );
}

export default TopSellers;
