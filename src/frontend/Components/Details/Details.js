import React, { useEffect, useState } from "react";

import ReadMore from "./ReadMore";
import TopSellers from "./TopSellers/TopSellers";
import { withRouter } from "react-router-dom";
// Redux Imports
import { useDispatch, useSelector } from "react-redux";
import {
  addToCart,
  getCart,
  getProductFrontend,
  setShowLoginModal,
  updateCartQty,
} from "../../../store/actions";

// Swiper Imports
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { imageLink } from "../../Helper/ImageLink";
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function Details(props) {
  const productSlug = props.match.params.productSlug;
  let detailProduct = useSelector((state) => state.ProductFrontend.product);
  let allcart = useSelector((state) => state.CartFrontend.cart);
  let [cart, setCart] = useState({});
  let [isAddToCart, setIsAddToCart] = useState(false);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.UserFrontend.userData);
  const [inputEmpty, setInputEmpty] = useState(false);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  useEffect(() => {
    dispatch(
      getProductFrontend({
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_permalink: productSlug,
      })
    );
  }, [productSlug, userData]);

  useEffect(() => {
    if (detailProduct[0]) {
      allcart.map((item) => {
        if (detailProduct[0].product_id === item.cart_product_id) {
          setCart(item);
          setIsAddToCart(true);
        } else {
          setCart({});
          setIsAddToCart(false);
        }
      });
    }
  }, [detailProduct[0], allcart]);

  const qtyMinus = () => {
    if (cart.cart_id) {
      var cartqty2 = cart.cart_quantity - 1;

      if (cartqty2 > 0) {
        // setCartQty(cartqty2);
        dispatch(updateCartQty({ id: cart.cart_id, new_quantity: cartqty2 }));
      }
    }
  };
  const qtyPlus = () => {
    if (cart.cart_id) {
      let cartqty2 = cart.cart_quantity;
      if (cartqty2 < detailProduct[0].product_moq) {
        cartqty2 = cart.cart_quantity + 1;
        // setCartQty(cartqty2);
        dispatch(updateCartQty({ id: cart.cart_id, new_quantity: cartqty2 }));
      }
    }
  };
  // Add to Cart Button Cliked
  const addToCartHandler = (e) => {
    if (userData.customer_id) {
      if (cart.cart_id == null) {
        const cart = {
          cart_customer_id: userData.customer_id,
          cart_product_id: detailProduct[0].product_id,
          cart_quantity: 1,
        };
        dispatch(addToCart(cart));
        setIsAddToCart(true);
      }
    } else {
      dispatch(setShowLoginModal());
    }
  };

  let handleChangeItemQty = (e) => {
    let value = e.target.value;
    if (!e.target.value) {
      setInputEmpty(true);
    } else {
      dispatch(updateCartQty({ id: cart.cart_id, new_quantity: value }));
      setInputEmpty(false);
    }
  };

  return (
    <div className="detailpage">
      {Object.keys(detailProduct[0]).length === 0 ? (
        <div>...Loading</div>
      ) : (
        <div class="details">
          <div class="container-fluid cmpad">
            <div class="row">
              <div class="prozoom">
                <Swiper
                  spaceBetween={10}
                  allowTouchMove={false}
                  effect="fade"
                  observer={true}
                  observeParents={true}
                  className="product-original"
                >
                  <SwiperSlide>
                    <span class="zoom" id="pro3">
                      <img
                        src={`${imageLink()}${
                          detailProduct[0].product_image_main
                        }`}
                        alt
                      />
                    </span>
                  </SwiperSlide>
                </Swiper>
              </div>
              <div class="prodetail">
                <div class="detinfo">
                  <h1 class="name">{detailProduct[0].product_name_english}</h1>
                  <div class="price">
                    <h2 class="amount">Rs. {detailProduct[0].product_sp}</h2>
                    <p class="cut">/{detailProduct[0].default_unit}</p>
                  </div>
                  <div class="detbutton">
                    <div
                      className="plusminus"
                      style={{ display: isAddToCart ? "block" : "none" }}
                    >
                      <form className="cartform">
                        <input
                          type="button"
                          onClick={qtyMinus}
                          className="qtyminus"
                          field="quantity"
                        ></input>
                        <input
                          type="text"
                          name="quantity"
                          className="qty"
                          value={inputEmpty ? "" : cart.cart_quantity}
                          onChange={handleChangeItemQty}
                        ></input>
                        <input
                          type="button"
                          onClick={qtyPlus}
                          className="qtyplus"
                          field="quantity"
                        ></input>
                      </form>
                    </div>
                    <button
                      onClick={addToCartHandler}
                      style={{
                        background:
                          detailProduct[0].stock_status === 1 && !isAddToCart
                            ? "#69c328"
                            : isAddToCart
                            ? "#f0f9e9"
                            : "#979797",
                      }}
                      class={isAddToCart ? "addcart selected" : "addcart"}
                      disabled={
                        detailProduct[0].stock_status === 1 ? false : true
                      }
                    >
                      <img src="../img/icons/tick.svg" alt="" />{" "}
                      <span>
                        {detailProduct[0].stock_status === 1
                          ? isAddToCart
                            ? "Added to Cart"
                            : "Add to Cart"
                          : "Not Available"}
                      </span>
                    </button>
                  </div>
                  <h4 class="dchead">Description</h4>

                  <p class="description toggle-text">
                    <ReadMore>
                      {detailProduct[0].product_description
                        ? detailProduct[0].product_description
                        : " "}
                    </ReadMore>
                  </p>
                  <span class="farmer">
                    Farmer Name:{" "}
                    <span>
                      {detailProduct[0].farmer_name
                        ? detailProduct[0].farmer_name
                        : "Unknown"}
                    </span>
                  </span>
                </div>
              </div>
            </div>
            <TopSellers product={detailProduct} />
          </div>
        </div>
      )}
    </div>
  );
}

export default withRouter(Details);
