import react, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import "./Header.css";
import { getBillAmount } from "../../Helper/CartCheckoutHelper";
import Autosuggest from "react-autosuggest";
import {
  getCommunitiesFrontend,
  setCommunityGlobally,
  setUserDataFrontend,
  getOrderWindow,
  getCategoriesFrontend,
  setShowLoginModal,
  getCart,
  setShowCart,
  getProductsFrontend,
  clearCart,
  setShowCommunityModalGlobally,
  setShowOrderWindowPopup,
} from "../../../store/actions";
import { useEffect } from "react";
import Timing from "../Timings/Timings";
import LoginModal from "../../Components/Login/LoginModal";
import CommunityModal from "../../Components/Community/CommunityModal";
import { Link, withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import ShareModal from "../Timings/ShareModal";
import { categoryImageLink } from "../../Helper/categoryImageLink";
import OrderWindowPopup from "./OrderWindowPopup";

function Header(props) {
  let history = useHistory();
  const location = useLocation();
  const [page, setPage] = useState("");
  const allProducts = useSelector((state) => state.ProductFrontend.allProducts);
  const userData = useSelector((state) => state.UserFrontend.userData);
  const communitySelected = useSelector(
    (state) => state.CommunityFrontend.communitySelected
  );
  const categories = useSelector((state) => state.CategoryFrontend.categories);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);
  const { showCommunityModalGlobally, showShareModal, showOrderWindowPopup } =
    useSelector((state) => state.UiFrontend);

  const showLoginModal = useSelector(
    (state) => state.UiFrontend.showLoginModal
  );
  const communities = useSelector(
    (state) => state.CommunityFrontend.communities
  );
  const [showCommunityModal, setShowCommunityModal] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const [cart, setCart] = useState([]);
  const [bannerColor, setBannerColor] = useState("");
  const [value, setValue] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  const dispatch = useDispatch();
  const cartGlobal = useSelector((state) => state.CartFrontend.cart);

  useEffect(() => {
    setPage(location.pathname);
  }, [location.pathname]);

  useEffect(() => {
    dispatch(getCart(userData.customer_id));
  }, [userData]);

  useEffect(() => {
    setCart(cartGlobal);
  }, [cartGlobal]);

  useEffect(() => {
    dispatch(getProductsFrontend());
    dispatch(getCommunitiesFrontend());
    dispatch(getCategoriesFrontend());

    let userInLocalStorage = JSON.parse(localStorage.getItem("userData"));

    if (userInLocalStorage) {
      dispatch(setUserDataFrontend(userInLocalStorage));
    }
  }, []);

  useEffect(() => {
    if (communitySelected.community_id) {
      dispatch(getOrderWindow(communitySelected.community_id));
    }
  }, [communitySelected]);

  useEffect(() => {
    if (userData.customer_community_id) {
      communities.map((item) => {
        if (userData.customer_community_id === item.community_id) {
          dispatch(setCommunityGlobally(item));
        }
      });
    } else {
      dispatch(clearCart());
      let communitySelectedInLocalStorage = JSON.parse(
        localStorage.getItem("communitySelected")
      );

      if (communitySelectedInLocalStorage) {
        dispatch(setCommunityGlobally(communitySelectedInLocalStorage));
      } else {
        setShowCommunityModal(true);
      }
    }
  }, [userData, communities]);

  let logout = () => {
    dispatch(setUserDataFrontend({}));
    props.history.push("/");
    setShowDropdown(false);
  };

  useEffect(() => {
    let startDayNumber = orderWindow.order_window_start_day_no;
    let endDayNumber = orderWindow.order_window_end_day_no;

    let daysAndNumbers = {
      Sun: 7,
      Mon: 1,
      Tue: 2,
      Wed: 3,
      Thu: 4,
      Fri: 5,
      Sat: 6,
    };
    let dateNumberToday = daysAndNumbers[moment().format("ddd")];

    if (startDayNumber <= dateNumberToday && dateNumberToday <= endDayNumber) {
      let startTime = orderWindow.order_window_start_time;
      let endTime = orderWindow.order_window_end_time;
      let timeNow = moment().format("HH:mm:ss");
      console.log(startTime, timeNow, endTime, "inside");

      if (startTime <= timeNow && timeNow <= endTime) {
        setBannerColor("green");
      }
    } else {
      setBannerColor("orange");
    }
  }, [orderWindow]);

  let onChange = (event, { newValue }) => {
    setValue(newValue);
  };

  const inputProps = {
    placeholder: "Search for Products...",
    value,
    className: "form-control",
    onChange: onChange,
  };

  let onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  let onSuggestionsFetchRequested = ({ value }) => {
    searchProducts(value);
  };

  let searchProducts = (value) => {
    fetch(`https://api-bhoomi.mykar.in/api/search`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        search_value: value,
        product_type: 0,
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          setSuggestions(result.data);
        } else {
        }
      });
  };

  const renderSuggestion = (suggestion) => (
    <div>
      <Link to={`/product/${suggestion.product_permalink}`}>
        {suggestion.product_name_english}
      </Link>
    </div>
  );

  const getSuggestionValue = (suggestion) => suggestion.product_name_english;

  const renderInputComponent = (inputProps) => (
    <div className="inputContainer">
      <input {...inputProps} />
      {value ? (
        <div
          className="iconRight"
          src="/img/icons/close.svg"
          onClick={() => directToSearch()}
        >
          Search
        </div>
      ) : null}
    </div>
  );

  let directToSearch = () => {
    props.history.push(`/search/${value}`);
  };

  let closeCommunityModalPermannent = () => {
    setShowCommunityModal(false);
    dispatch(setShowCommunityModalGlobally(false));
  };

  let showCommunityModalPermanantlly = () => {
    setShowCommunityModal(true);
    dispatch(setShowCommunityModalGlobally(true));
  };
  console.log(orderWindow, "owp");
  return (
    <div>
      <header className="header">
        <div class="container-fluid headtop">
          <div class="cmpad">
            <div class="container-fluid">
              <div class="row mainrow">
                <div class="logo" style={{ padding: 0 }}>
                  <Link to="/" class="d-none d-sm-block">
                    <img src="/img/logo/logo.svg" alt="Bhoomi Farmers" />
                  </Link>
                  <Link to="/" class="d-block d-sm-none">
                    <img src="/img/logo/icon.svg" alt="Bhoomi Farmers" />
                  </Link>
                </div>

                <div class="search">
                  <form action="">
                    <div class="input-group">
                      <div
                        class="input-group-btn search-panel"
                        data-search="students"
                      >
                        <button
                          type="button"
                          class="btn btn-default dropdown-toggle"
                          data-bs-toggle="modal"
                          data-bs-target="#community"
                          disabled={
                            history.location.pathname === "/profile" ||
                            userData.customer_community_id
                              ? true
                              : false
                          }
                          onClick={() => showCommunityModalPermanantlly()}
                        >
                          <span class="deltitle">Delivery to</span>
                          <img src="/img/icons/mapmarker.svg" alt />
                          <span id="commName" class="search_by">
                            {communitySelected.community_name
                              ? communitySelected.community_name
                              : "Not Available"}
                          </span>
                        </button>
                      </div>
                      <span class="input-group-btn">
                        <button class="btn btn-default btnsrch" type="button">
                          <img src="/img/icons/search.svg" alt />
                        </button>
                      </span>

                      <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={
                          onSuggestionsFetchRequested
                        }
                        onSuggestionsClearRequested={
                          onSuggestionsClearRequested
                        }
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                        renderInputComponent={renderInputComponent}
                      />
                    </div>
                  </form>
                </div>

                <div class="topmenu">
                  <ul>
                    <li onClick={() => dispatch(setShowCart())}>
                      <a class="btncart">
                        <span>
                          <span class="topmenu__cartqty">{cart.length}</span>
                          <img src="/img/icons/cart.svg" alt />
                        </span>
                        <span class="mcinfo" style={{ display: "inline-grid" }}>
                          My Cart
                          <span className="topmenu__carttotal">
                            Rs. {getBillAmount(cart, allProducts)}
                          </span>
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>

                {userData.customer_name ? (
                  <div
                    class="actionbtn user"
                    onClick={() => setShowDropdown(!showDropdown)}
                  >
                    <div className="user-icon">
                      <img src="/img/icons/user.svg" />
                    </div>
                    <div>
                      <h4>{userData.customer_name}</h4>
                      <p>{userData.customer_mob}</p>
                    </div>
                    <div className="down-icon">
                      <img src="/img/icons/down.svg" />
                    </div>
                  </div>
                ) : (
                  <div class="actionbtn">
                    <button
                      type="button"
                      onClick={() => dispatch(setShowLoginModal())}
                    >
                      Log In
                    </button>
                  </div>
                )}
                {showDropdown ? (
                  <div className="dropdown">
                    <li>
                      <Link to="/profile">
                        <img src="/img/icons/profile.svg" />
                        My profile
                      </Link>
                    </li>
                    <li>
                      <Link to="/orders">
                        <img src="/img/icons/order.png" />
                        My orders
                      </Link>
                    </li>
                    <li onClick={() => logout()}>
                      <img src="/img/icons/logout.png" />
                      Logout
                    </li>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>

        <div class="container-fluid headbottom">
          <div class="cmpad">
            <div class="container-fluid">
              <div class="row mainrow">
                <div class="mainmenu">
                  <div class="scrollicon">
                    <a href="/">
                      <img src="/img/logo/icon.svg" alt />
                    </a>
                  </div>

                  <div class="normalmenu">
                    <Link
                      class={`mainli ${
                        page === "/best-sellers" || page === "/"
                          ? "active"
                          : null
                      }`}
                      to="/best-sellers"
                    >
                      <img src="/img/icons/star.svg" alt /> Best Sellers
                    </Link>
                  </div>
                  {console.log(categories, "cat")}
                  {categories.map((category) => {
                    return (
                      <div class="normalmenu">
                        <Link
                          class={`mainli ${
                            page === "/" + category.category_permalink
                              ? "active"
                              : null
                          }`}
                          to={`/${category.category_permalink}`}
                        >
                          <img
                            src={`${categoryImageLink()}${
                              category.category_icon_svg
                            }`}
                            alt
                          />{" "}
                          {category.category_name}
                        </Link>
                      </div>
                    );
                  })}
                </div>
                <Timing orderWindow={orderWindow} />
                {showShareModal ? <ShareModal /> : null}
              </div>
            </div>
          </div>
        </div>
      </header>
      {communitySelected.community_id && bannerColor ? (
        <div class={`orderwindow ${bannerColor}`}>
          <p>
            Your Order window: {orderWindow.order_window_start_day}{" "}
            {moment(orderWindow.order_window_start_time, "HH:mm:ss").format(
              "hh:mm A"
            )}{" "}
            to {orderWindow.order_window_end_day}{" "}
            {moment(orderWindow.order_window_end_time, "HH:mm:ss").format(
              "hh:mm A"
            )}
          </p>
        </div>
      ) : null}

      <div class="resnav d-md-none">
        <ul class="resmenu">
          <li class="active">
            <Link to="/">
              <img src="/img/icons/reshome.svg" alt />
              <span>Home</span>
            </Link>
          </li>
          <li onClick={() => dispatch(setShowCart())}>
            <a class="btncart">
              <span>
                <span class="topmenu__cartqty mob">{cart.length}</span>
                <img src="/img/icons/rescart.svg" alt />
              </span>
              <span class="mcinfo" style={{ display: "inline-grid" }}>
                Cart
              </span>
            </a>
          </li>
          {userData.customer_name ? (
            <li onClick={() => setShowDropdown(!showDropdown)}>
              <a data-toggle="modal" data-target="#loginModal">
                <img src="/img/icons/reslogin.svg" alt />
                <span>{userData.customer_name ? "Profile" : "Login"}</span>
              </a>
            </li>
          ) : (
            <li onClick={() => dispatch(setShowLoginModal())}>
              <a data-toggle="modal" data-target="#loginModal">
                <img src="/img/icons/reslogin.svg" alt />
                <span>Login</span>
              </a>
            </li>
          )}
        </ul>
      </div>

      {showOrderWindowPopup ? <OrderWindowPopup /> : null}

      <div class="resdelivery d-md-none">
        <p>
          Delivery date <span>Tue, 21 Aug</span>
        </p>
        {/* <button onClick={() => dispatch(setShowOrderWindowPopup())}>
          Order Info
        </button> */}
      </div>

      {/* Login Modal */}

      {showLoginModal ? <LoginModal /> : null}

      {showCommunityModal && showCommunityModalGlobally ? (
        <CommunityModal setShowCommunityModal={closeCommunityModalPermannent} />
      ) : null}
    </div>
  );
}

export default withRouter(Header);
