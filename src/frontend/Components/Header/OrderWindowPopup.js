import React from "react";
import { Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { setHideOrderWindowPopup } from "../../../store/actions";
import moment from "moment";

export default function OrderWindowPopup() {
  const dispatch = useDispatch();

  return (
    <Modal
      show={true}
      onHide={() => dispatch(setHideOrderWindowPopup())}
      className="modal modalbottom fade show deldateModal"
    >
      <Modal.Body>
        <button
          type="button"
          class="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={() => dispatch(setHideOrderWindowPopup())}
        >
          <img src="/img/icons/close.svg" />
        </button>
        <h4 class="modal-title mt-5" id="deldateModalLabel">
          Order Info
        </h4>
        {/* <div class="customcheck">
          <form action="#">
            {orderWindow.delivery_date.map((item, index) => {
              return (
                <div>
                  <input
                    type="radio"
                    id="deldt1"
                    name="radio-group"
                    checked
                    disabled
                  />
                  <label for="deldt1">
                    {moment(item.date).format("ddd, DD MMM")}
                  </label>
                  <span>
                    Your Order window: <b>Sunday 10 AM to Monday 8 PM</b>
                  </span>
                </div>
              );
            })}
          </form>
        </div> */}
      </Modal.Body>
    </Modal>
  );
}
