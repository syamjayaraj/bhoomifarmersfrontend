import React from "react";
import { Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import {
  WhatsappShareButton,
  FacebookShareButton,
  EmailShareButton,
} from "react-share";
import { setShowShareModal } from "../../../store/actions";

export default function ShareModal() {
  let dispatch = useDispatch();
  return (
    <Modal
      show={true}
      onHide={() => dispatch(setShowShareModal(false))}
      className="modal fade share-modal"
    >
      <Modal.Body>
        <button
          type="button"
          className="close"
          data-bs-dismiss="modal"
          aria-label="Close"
          onClick={() => dispatch(setShowShareModal(false))}
        >
          <img src="/img/icons/close.svg" alt />
        </button>
        <div className="icon-container">
          <WhatsappShareButton
            size={32}
            round={true}
            url={"https://bhoomifarm.in"}
          >
            <img src="/img/icons/whatsapp.png" />
          </WhatsappShareButton>
          <FacebookShareButton
            iconFillColor="black"
            url={"https://bhoomifarm.in"}
          >
            <img src="/img/icons/facebook.png" />
          </FacebookShareButton>
          <EmailShareButton url={"https://bhoomifarm.in"}>
            <img src="/img/icons/email.png" />
          </EmailShareButton>
        </div>
      </Modal.Body>
    </Modal>
  );
}
