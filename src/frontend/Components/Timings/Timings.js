import React from "react";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { setShowShareModal } from "../../../store/actions";

function Timing(props) {
  let dispatch = useDispatch();
  let { orderWindow } = props;

  if (orderWindow.delivery_date && orderWindow.delivery_date.length !== 0) {
    return (
      <div class="delivery customcheck">
        <span class="deldt">Next delivery date</span>
        <form action="#">
          {orderWindow.delivery_date.map((item, index) => {
            return (
              <div>
                <input
                  type="radio"
                  id="test1"
                  name="radio-group"
                  checked={index === 0 ? true : false}
                  disabled
                />
                <label for="test1" style={{ cursor: "default" }}>
                  {moment(item.date).format("ddd, DD MMM")}
                </label>
              </div>
            );
          })}
        </form>
      </div>
    );
  } else {
    return (
      <button
        className="share-button"
        onClick={() => dispatch(setShowShareModal(true))}
      >
        <img src="/img/icons/share.png" className="share-button-icon" />
        Share the website
      </button>
    );
  }
}

export default Timing;
