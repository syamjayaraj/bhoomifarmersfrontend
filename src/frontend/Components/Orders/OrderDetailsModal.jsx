import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import moment from "moment";
import Grid from "@material-ui/core/Grid";
import { Table } from "@material-ui/core";
import { TableBody } from "@material-ui/core";
import { TableCell } from "@material-ui/core";
import { TableContainer } from "@material-ui/core";
import { TableHead } from "@material-ui/core";
import { TableRow } from "@material-ui/core";
import { Paper } from "@material-ui/core";

function OrderDetailsModal(props) {
  let { closeModal, order } = props;

  let orderStatus =
    order.o_approved_status === 0
      ? "Received"
      : order.o_approved_status === 1
      ? "Out for delivery"
      : order.o_approved_status === 2
      ? "Delivered"
      : order.o_approved_status === 3
      ? "Canceled"
      : null;

  let paymentStatus = order.o_pay_status === 0 ? "Not paid" : "Paid";

  return (
    <Modal
      show={true}
      onHide={() => closeModal(false)}
      className="modal fade community show"
    >
      <Modal.Header>
        <Modal.Title>Order Details</Modal.Title>
        <button
          type="button"
          class="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={() => closeModal(false)}
        >
          <img src="/img/icons/close.svg" alt />
        </button>
      </Modal.Header>
      <Modal.Body>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "1rem",
          }}
        >
          <div></div>
          <div>
            <h6>
              Order# {order.o_number}{" "}
              <span
                style={{
                  background: order.o_pay_status === 0 ? "red" : "green",
                  padding: "0.2rem",
                  borderRadius: ".2rem",
                  fontSize: ".8em",
                  color: "white",
                }}
              >
                {paymentStatus}
              </span>
            </h6>
            <h6>Order Status: {orderStatus}</h6>
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div
            style={{
              marginTop: "1rem",
            }}
          >
            <p>Billed To:</p>
            <h6>{order.customer.customer_name}</h6>
            <h6>{order.customer.customer_mob}</h6>
            <h6>{order.customer.customer_email}</h6>
            <h6>{order.Community.community_name}</h6>
          </div>
          <div>
            <paymentStatus>Order Date:</paymentStatus>
            <h6>{moment(order.o_date).format("DD-MM-YYYY")}</h6>
            <p>Order Time:</p>
            <h6>{order.o_time}</h6>
          </div>
        </div>
        <div style={{ marginTop: "2rem" }}>
          Order summary
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    SKU #
                  </TableCell>
                  <TableCell
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    Item
                  </TableCell>
                  <TableCell
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    Price
                  </TableCell>
                  <TableCell
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    Qty
                  </TableCell>
                  <TableCell
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    Total
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {order.orderItems &&
                  order.orderItems.map((item) => {
                    return (
                      <TableRow>
                        <TableCell>
                          {item.product && item.product.product_sku_autogen}
                        </TableCell>
                        <TableCell>{item.o_item_name}</TableCell>
                        <TableCell>{item.o_item_sp}</TableCell>
                        <TableCell>{item.o_item_qty}</TableCell>
                        <TableCell>{item.o_item_original_amount}</TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </Modal.Body>
    </Modal>
  );
}
export default withRouter(OrderDetailsModal);
