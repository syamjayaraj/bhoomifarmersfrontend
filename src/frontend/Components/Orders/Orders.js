import React, { useEffect, useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./MyOrders.css";
import Grid from "@material-ui/core/Grid";
import { Table } from "@material-ui/core";
import { TableBody } from "@material-ui/core";
import { TableCell } from "@material-ui/core";
import { TableContainer } from "@material-ui/core";
import { TableHead } from "@material-ui/core";
import { TableRow } from "@material-ui/core";
import { Paper } from "@material-ui/core";
import moment from "moment";
import OrderDetailsModal from "./OrderDetailsModal";

export default function MyOrders() {
  const [orders, setOrders] = useState([]);
  const [order, setOrder] = useState({
    customer: {},
    Community: {},
  });
  const [showOrderDetailsModal, setShowOrderDetailsModal] = useState(false);

  const userData = useSelector((state) => state.UserFrontend.userData);

  useEffect(() => {
    getOrders();
  }, [userData]);

  let getOrders = () => {
    fetch(
      `https://api-bhoomi.mykar.in/api/view_orders/${userData.customer_id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          setOrders(result.data);
        } else {
        }
      });
  };

  let getOrderDetails = (orderId) => {
    if (order.o_id === orderId) {
      setOrder({});
    } else {
      fetch(`https://api-bhoomi.mykar.in/api/order_details/${orderId}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            setOrder(result.data);
            setShowOrderDetailsModal(true);
          } else {
          }
        });
    }
  };

  console.log(order.orderItems, "orderItems");

  let showOrderDetails = (orderId) => {
    getOrderDetails(orderId);
  };

  let closeModal = () => {
    setOrder({
      customer: {},
      Community: {},
    });
    setShowOrderDetailsModal(false);
  };

  return (
    <div className="container">
      {showOrderDetailsModal ? (
        <OrderDetailsModal order={order} closeModal={closeModal} />
      ) : null}

      <h1
        style={{ paddingBottom: 20, marginTop: "5%", fontFamily: "Quicksand" }}
      >
        My Orders
      </h1>
      <Grid container spacing={2}>
        <Grid item xs={12} md={12} lg={8} sm container>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{
                      fontFamily: "Quicksand",
                      fontStyle: "normal",
                      fontWeight: 600,
                      fontSize: "22px",
                      lineHeight: "27px",
                    }}
                  >
                    Date
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: "Quicksand",
                      fontStyle: "normal",
                      fontWeight: 600,
                      fontSize: "22px",
                      lineHeight: "27px",
                    }}
                  >
                    Time
                  </TableCell>

                  <TableCell
                    style={{
                      fontFamily: "Quicksand",
                      fontStyle: "normal",
                      fontWeight: 600,
                      fontSize: "22px",
                      lineHeight: "27px",
                    }}
                  >
                    Order Id
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: "Quicksand",
                      fontStyle: "normal",
                      fontWeight: 600,
                      fontSize: "22px",
                      lineHeight: "27px",
                    }}
                  >
                    Amount
                  </TableCell>
                  <TableCell
                    style={{
                      fontFamily: "Quicksand",
                      fontStyle: "normal",
                      fontWeight: 600,
                      fontSize: "22px",
                      lineHeight: "27px",
                    }}
                  >
                    Status
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {console.log(orders, "orders")}
                {orders.map((item) => {
                  let orderStatus =
                    item.o_approved_status === 0
                      ? "Received"
                      : item.o_approved_status === 1
                      ? "Out for delivery"
                      : item.o_approved_status === 2
                      ? "Delivered"
                      : item.o_approved_status === 3
                      ? "Canceled"
                      : null;
                  return (
                    <TableRow onClick={() => showOrderDetails(item.o_id)}>
                      <TableCell
                        style={{
                          fontFamily: "Quicksand",
                          fontStyle: "normal",
                          fontWeight: "normal",
                          fontSize: "18px",
                          lineHeight: "22px",
                        }}
                      >
                        {moment(item.o_date).format("DD-MM-YYYY")}
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: "Quicksand",
                          fontStyle: "normal",
                          fontWeight: "normal",
                          fontSize: "18px",
                          lineHeight: "22px",
                        }}
                      >
                        {item.o_time}
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: "Quicksand",
                          fontStyle: "normal",
                          fontWeight: "normal",
                          fontSize: "18px",
                          lineHeight: "22px",
                          color: "#005FED",
                        }}
                      >
                        {item.o_number}
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: "Quicksand",
                          fontStyle: "normal",
                          fontWeight: "normal",
                          fontSize: "18px",
                          lineHeight: "22px",
                        }}
                      >
                        Rs. {item.o_total}
                      </TableCell>
                      <TableCell
                        style={{
                          fontFamily: "Quicksand",
                          fontStyle: "normal",
                          fontWeight: "normal",
                          fontSize: "18px",
                          lineHeight: "22px",
                          color: "#C46404",
                        }}
                      >
                        {orderStatus}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}
