import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  addToCart,
  updateCartQty,
  setShowLoginModal,
} from "../../../store/actions";
import { imageLink } from "../../Helper/ImageLink";

function Probox(props) {
  let { product } = props;
  const [qty, setQty] = useState(1);
  const [isActive, setActive] = useState("cartform");
  const [addToCartBtnDisplay, setAddToCartBtnDisplay] = useState("block");
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.UserFrontend.userData);

  // const [cartqty, setCartQty] = useState(0);

  useEffect(() => {
    if (props.cart == null) {
      setActive("cartform");
      setAddToCartBtnDisplay("block");
    } else {
      setActive("cartform active");
      setAddToCartBtnDisplay("none");
      // setCartQty(props.cart.cart_quantity);
    }
  }, [props]);

  const qtyMinus = () => {
    var cartqty2 = props.cart.cart_quantity - 1;

    if (cartqty2 > 0) {
      // setCartQty(cartqty2);
      dispatch(
        updateCartQty({ id: props.cart.cart_id, new_quantity: cartqty2 })
      );
    }
  };
  const qtyPlus = () => {
    let cartqty2 = props.cart.cart_quantity;
    if (cartqty2 < product.product_moq) {
      cartqty2 = props.cart.cart_quantity + 1;
      // setCartQty(cartqty2);
      dispatch(
        updateCartQty({ id: props.cart.cart_id, new_quantity: cartqty2 })
      );
    }
  };

  // Add to Cart Button Cliked
  const addToCartHandler = (e) => {
    if (userData.customer_id) {
      if (props.cart === null) {
        const cart = {
          cart_customer_id: userData.customer_id,
          cart_product_id: props.id,
          cart_quantity: 1,
        };
        dispatch(addToCart(cart));
        setActive("cartform active");
        setAddToCartBtnDisplay("none");
      }
    } else {
      dispatch(setShowLoginModal());
    }
  };
  let stylef = { display: "block" };
  if (props.stockout === true && props.allproactive === true) {
    stylef = { display: "block" };
  } else if (props.stockout === true && props.allproactive === false) {
    stylef = { display: "none" };
  }

  return (
    <div
      className={props.stockout ? "procol stockout" : "procol"}
      style={stylef}
    >
      <div className="probox" id="probox1">
        <Link
          to={`/product/${product && product.product_permalink}`}
          className="proimg"
        >
          <img src={`${imageLink()}${props.image}`} alt=""></img>
        </Link>
        <div className="procont">
          <Link to={`/product/${product && product.product_permalink}`}>
            <h5 className="name">{props.name}</h5>
          </Link>
          <div className="addtocart">
            <p className="price">
              Rs. {product && product.product_sp}
              <span className="cut">/{product && product.default_unit}</span>
            </p>
            <div className="plusminus">
              <form className={isActive}>
                <input
                  type="button"
                  className="qtyminus"
                  field="quantity"
                  onClick={qtyMinus}
                ></input>
                <input
                  type="text"
                  name="quantity"
                  value={props.cart ? props.cart.cart_quantity : 0}
                  className="qty"
                  readOnly
                ></input>
                <input
                  type="button"
                  className="qtyplus"
                  field="quantity"
                  onClick={qtyPlus}
                ></input>
              </form>
            </div>
            <div className="addcol">
              <button
                className="addbtn"
                id="addbtn"
                style={{
                  background: props.stock_status === 1 ? "#69c328" : "#979797",
                  display: addToCartBtnDisplay,
                }}
                onClick={addToCartHandler}
                disabled={props.stock_status === 1 ? false : true}
              >
                {props.stock_status === 1 ? "Add to Cart" : "Not Available"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Probox;
