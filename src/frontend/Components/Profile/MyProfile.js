import React, { useState, useEffect } from "react";
import "./MyProfile.css";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Select, MenuItem, FormControl, InputLabel } from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import toastr from "toastr";
import { setUserDataFrontend } from "../../../store/actions";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  notchedOutline: {
    borderColor: "#C46404",
    borderWidth: 1,
    "&:hover": {
      borderColor: "#C46404",
      borderWidth: 2,
    },
  },
}));
const styles = {
  floatingLabelFocusStyle: {
    color: "#C46404",
  },
};
const currencies = [
  {
    value: "Adarsh Palm Retreat - Phase 1",
    label: "Adarsh Palm Retreat - Phase 1",
  },
];

export default function Profile() {
  let history = useHistory();

  const classes = useStyles();
  const dispatch = useDispatch();

  const communities = useSelector(
    (state) => state.CommunityFrontend.communities
  );

  const [profile, setProfile] = useState({
    Community: {
      community_id: 1,
    },
  });

  const userData = useSelector((state) => state.UserFrontend.userData);

  useEffect(() => {
    getProfile();
  }, [userData]);

  let getProfile = () => {
    fetch(
      `https://api-bhoomi.mykar.in/api/customerProfile/${userData.customer_id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          setProfile(result.data);
          // dispatch(setUserDataFrontend(result.data));
        } else {
        }
      });
  };

  let handleChangeInput = (e) => {
    setProfile({
      ...profile,
      [e.target.name]: e.target.value,
    });
  };

  const handleChangeCommunity = (event) => {
    communities.map((community) => {
      if (community.community_id === event.target.value) {
        setProfile({
          ...profile,
          Community: community,
        });
      }
    });
  };

  console.log(userData, "lll");

  let editProfile = () => {
    fetch("https://api-bhoomi.mykar.in/api/editProfile", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        customer_id: profile.customer_id,
        customer_name: profile.customer_name,
        customer_mob: profile.customer_mob,
        customer_email: profile.customer_email,
        customer_community_id: profile.Community.community_id,
        customer_apartment_no: profile.customer_apartment_no,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        console.log(result, "result1");
        let data = result.data;
        if (result.success) {
          let param = {
            ...userData,
            ...data,
          };
          dispatch(setUserDataFrontend(param));

          toastr.success("Profile updated successfully");
        } else {
          toastr.error(result.message);
        }
      });
  };

  console.log(profile, "pro");

  let routeToHome = () => {
    history.push("/");
  };

  return (
    <div className="container" style={{ marginBottom: "2rem" }}>
      <h1
        style={{ paddingBottom: 20, marginTop: "5%", fontFamily: "Quicksand" }}
      >
        My Profile
      </h1>
      <h5
        className="my-profile-h5"
        style={{ paddingBottom: 20, fontFamily: "Quicksand" }}
      >
        Account & Address Details
      </h5>

      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="standard-basic"
          label="Your Name"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
            shrink: profile.customer_name ? true : false,
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          onChange={handleChangeInput}
          variant="outlined"
          name="customer_name"
          value={profile.customer_name}
        />

        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="Your email Address"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
            shrink: profile.customer_email ? true : false,
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
          name="customer_email"
          value={profile.customer_email}
          onChange={handleChangeInput}
        />
        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="Phone Number"
          InputLabelProps={{
            style: {
              color: "#C46404",
              // background: "#F5F6F8",
              // marginTop: "-.2rem",
              // paddingLeft: ".1rem",
              // paddintRight: ".1rem",
            },
            shrink: profile.customer_mob ? true : false,
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
          name="customer_mob"
          value={profile.customer_mob}
          onChange={handleChangeInput}
        />
        <br />

        <FormControl
          variant="outlined"
          className={classes.formControl}
          style={{ width: "41ch", marginTop: "4%" }}
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          value={"hai"}
        >
          <InputLabel
            id="demo-simple-select-outlined-label"
            style={{
              color: "#C46404",
            }}
            shrink={true}
          >
            Apartment Name
          </InputLabel>

          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={profile.Community.community_id}
            onChange={handleChangeCommunity}
            label="Apartment Name"
          >
            {communities.map((comm) => {
              return (
                <MenuItem key={comm.community_id} value={comm.community_id}>
                  {comm.community_name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="Apartment Number"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
            shrink: profile.customer_apartment_no ? true : false,
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
          name="customer_apartment_no"
          value={profile.customer_apartment_no}
          onChange={handleChangeInput}
        />
        {/* 

        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="City"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
        />
        <br />
        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="State"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
        />
        <TextField
          style={{ width: "41ch", marginTop: "4%" }}
          id="outlined-basic"
          label="Pin Code"
          InputLabelProps={{
            style: {
              color: "#C46404",
            },
          }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          variant="outlined"
        /> */}

        <br />

        <Button
          variant="black"
          style={{
            fontFamily: "Quicksand",
            fontStyle: "normal",
            fontWeight: "normal",
            fontSize: "18px",
            lineHeight: "22px",
            width: "8%",
            marginTop: "4%",
            textTransform: "capitalize",
          }}
          onClick={() => routeToHome()}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          style={{
            backgroundColor: "#69C328",
            color: "#fff",
            borderRadius: "8px",
            fontFamily: "Quicksand",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: "18px",
            lineHeight: "38px",
            width: "fit-content",
            marginTop: "4%",
            textTransform: "capitalize",
          }}
          onClick={editProfile}
        >
          Save Changes
        </Button>
      </form>
    </div>
  );
}
