import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setCommunityGlobally } from "../../../store/actions";
import { Modal } from "react-bootstrap";
import SearchInput, { createFilter } from "react-search-input";

export default function CommunityModal(props) {
  const userData = useSelector((state) => state.UserFrontend.userData);

  let { setShowCommunityModal } = props;

  const communities = useSelector(
    (state) => state.CommunityFrontend.communities
  );

  const [modalState, setModalState] = useState("community");
  const [customer, setCustomer] = useState({});

  const dispatch = useDispatch();

  const [value, setValue] = useState("");

  let selectSuggestion = (item) => {
    if (item) {
      dispatch(setCommunityGlobally(item));
      setShowCommunityModal(false);
    }
  };

  useEffect(() => {
    communities.map((comm) => {
      if (comm.community_id === userData.customer_community_id) {
        setCommunityGlobally(comm);
      }
    });
  }, [userData]);

  let handleChangeCustomer = (e) => {
    setCustomer({
      ...customer,
      [e.target.name]: e.target.value,
    });
  };

  let saveAddress = () => {
    fetch("https://api-bhoomi.mykar.in/api/add_communityaddress", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        request_name: customer.name,
        request_email: customer.email,
        request_address1: customer.address1,
        request_address2: customer.address2,
        request_pincode: customer.pincode,
        request_city: customer.city,
        request_state_id: customer.state,
        request_mobile: customer.mobile,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          setModalState("thank");
        } else {
        }
      });
  };

  let searchUpdated = (term) => {
    setValue(term);
  };
  const KEYS_TO_FILTERS = ["community_name"];

  const filteredCommunities = communities.filter(
    createFilter(value, KEYS_TO_FILTERS)
  );

  return (
    <Modal
      show={true}
      onHide={() => setShowCommunityModal(false)}
      className="modal fade community show regThanq"
    >
      <Modal.Body>
        {modalState === "community" ? (
          <div>
            <button
              type="button"
              className="close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => setShowCommunityModal(false)}
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <h4 className="modal-title mt-5" id="communityLabel">
              Select your Community
            </h4>

            <SearchInput
              className="search-input"
              onChange={searchUpdated}
              inputClassName={"form-control"}
            />
            <ul className="community-menu">
              {filteredCommunities.map((item) => {
                return (
                  <li
                    className="from"
                    onClick={() => selectSuggestion(item)}
                    key={item.id}
                  >
                    <a>{item.community_name}</a>
                  </li>
                );
              })}
            </ul>

            <p className="find">
              Didn’t find your community?{" "}
              <a
                href="#"
                class="addAddressBtn"
                data-bs-toggle="modal"
                data-bs-target="#addAddress"
              >
                <img src="/img/icons/plusround.svg" alt />
                <a
                  data-bs-dismiss="modal"
                  onClick={() => setModalState("address")}
                >
                  Add your address
                </a>
              </a>
            </p>
          </div>
        ) : modalState === "address" ? (
          <div>
            <button
              type="button"
              class="close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => setShowCommunityModal(false)}
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <h4 class="modal-title mt-5" id="addAddressLabel">
              Add your address
            </h4>

            <div class="form-row">
              <div class="col-6 input-wrapper">
                <input
                  id="addrName"
                  class="intxt"
                  type="text"
                  id="user"
                  required
                  name="name"
                  onChange={handleChangeCustomer}
                  value={customer.name}
                />
                <label class="inplbl" for="user">
                  Full Name
                </label>
              </div>
              <div class="col-6 input-wrapper">
                <input
                  id="addrEmail"
                  class="intxt"
                  type="text"
                  required
                  name="email"
                  onChange={handleChangeCustomer}
                  value={customer.email}
                />
                <label class="inplbl" for="user">
                  Email Address
                </label>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  id="addrLine1"
                  class="intxt"
                  type="text"
                  required
                  name="mobile"
                  onChange={handleChangeCustomer}
                  value={customer.mobile}
                />
                <label class="inplbl" for="user">
                  Mobile Number
                </label>
              </div>
            </div>

            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  id="addrLine1"
                  class="intxt"
                  type="text"
                  required
                  name="address1"
                  onChange={handleChangeCustomer}
                  value={customer.address1}
                />
                <label class="inplbl" for="user">
                  Address Line 1
                </label>
              </div>
            </div>

            <div class="form-row">
              <div class="col-12 input-wrapper">
                <input
                  id="addrLine2"
                  class="intxt"
                  type="text"
                  required
                  name="address2"
                  onChange={handleChangeCustomer}
                  value={customer.address2}
                />
                <label class="inplbl" for="user">
                  Address Line 2
                </label>
              </div>
            </div>

            <div class="form-row">
              <div class="col-4 input-wrapper">
                <input
                  id="addrPin"
                  class="intxt"
                  type="text"
                  required
                  name="pincode"
                  onChange={handleChangeCustomer}
                  value={customer.pincode}
                />
                <label class="inplbl" for="user">
                  Pincode
                </label>
              </div>
              <div class="col-4 input-wrapper">
                <input
                  id="addrCity"
                  class="intxt"
                  type="text"
                  required
                  name="city"
                  onChange={handleChangeCustomer}
                  value={customer.city}
                />
                <label class="inplbl" for="user">
                  City
                </label>
              </div>
              <div class="col-4 input-wrapper">
                <input
                  id="addrState"
                  class="intxt"
                  type="text"
                  required
                  name="state"
                  onChange={handleChangeCustomer}
                  value={customer.state}
                />
                <label class="inplbl" for="user">
                  State
                </label>
              </div>
            </div>

            <div class="form-row">
              <div class="col-12 modbtncol">
                <button
                  class="btn btncancel"
                  data-bs-dismiss="modal"
                  onClick={() => setModalState("community")}
                >
                  Cancel
                </button>
                <button
                  id="addAddressNext"
                  class="btn btnsave"
                  onClick={() => saveAddress()}
                >
                  Save Address
                </button>
              </div>
            </div>
          </div>
        ) : modalState === "thank" ? (
          <div>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <img src="/img/icons/close.svg" alt />
            </button>
            <img src="/img/logo/logo.svg" class="bhlogo mt-5" alt />
            <h4 class="modal-title text-center mt-5" id="regThanqLabel">
              Thank you for registering your address with us!
            </h4>

            <div class="form-row">
              <div class="col-12 text-center">
                <p>We will get back to you and we will serve you soon.</p>
              </div>
            </div>

            <div class="form-row">
              <div class="col-12 text-center">
                <p>
                  <a href="#" class="share">
                    <i class="fa fa-share-alt"></i> Share
                  </a>{" "}
                  our website to your friends and family.
                </p>
              </div>
            </div>

            <div class="form-row mt-3">
              <div class="col-12 modbtncol">
                <button
                  id="otpNext"
                  class="btn btnsave w-100"
                  onClick={() => setShowCommunityModal(false)}
                >
                  Explore Our Products
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </Modal.Body>
    </Modal>
  );
}
