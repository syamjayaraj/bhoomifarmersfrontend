import "./App.css";

import Footer from "./Components/Footer/Footer";
import Header from "./Components/Header/Header";
import ProductList from "./Components/ProductList/ProductList";

import { useEffect, useState } from "react";
import Cart from "./Components/Cart/Cart";
import { withRouter } from "react-router";
import { getCategoryWiseProductsFrontend } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";

function Category(props) {
  const dispatch = useDispatch();

  const userData = useSelector((state) => state.UserFrontend.userData);
  const [deliverableActive, setDeliverableActive] = useState(false);
  const [allProActive, setAllProActive] = useState(false);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  const categories = useSelector((state) => state.CategoryFrontend.categories);

  const [isCartVisible, setIsCartVisible] = useState(false);
  const cartViewHandler = (e) => {
    if (isCartVisible) setIsCartVisible(false);
    else setIsCartVisible(true);
  };
  const [categorySelected, setCategorySelected] = useState({});

  useEffect(() => {
    categories &&
      categories.map((cat) => {
        if (props.match.params.categoryId === cat.category_permalink) {
          setCategorySelected(cat);
          if (userData.customer_id) {
            deliverableHandler(cat);
          } else {
            allProductHandler(cat);
          }
        }
      });
  }, [props.match.params.categoryId, categories, userData, orderWindow]);

  const deliverableHandler = (cat) => {
    let catTemp = cat.category_id ? cat : categorySelected;

    console.log(catTemp, "catTe1");

    if (deliverableActive === false) {
      setDeliverableActive(true);
      setAllProActive(false);
    }
    if (catTemp.category_id)
      dispatch(
        getCategoryWiseProductsFrontend({
          delivery_date:
            orderWindow &&
            orderWindow.delivery_date &&
            orderWindow.delivery_date[0] &&
            orderWindow.delivery_date[0].date,
          product_type: 0,
          category_id: catTemp.category_id,
        })
      );
  };

  const allProductHandler = (cat) => {
    let catTemp = cat.category_id ? cat : categorySelected;
    if (allProActive === false) {
      setDeliverableActive(false);
      setAllProActive(true);
    }
    console.log(catTemp, "catTe");

    if (catTemp.category_id)
      dispatch(
        getCategoryWiseProductsFrontend({
          delivery_date:
            orderWindow &&
            orderWindow.delivery_date &&
            orderWindow.delivery_date[0] &&
            orderWindow.delivery_date[0].date,
          product_type: 1,
          category_id: catTemp.category_id,
        })
      );
  };

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, [props.match.params.categoryId]);

  return (
    <div>
      <Header cartviewhandler={cartViewHandler} />
      <Cart iscartvisible={isCartVisible} cartviewhandler={cartViewHandler} />

      <ProductList
        title={categorySelected.category_name}
        categorySelected={categorySelected}
        deliverableHandler={deliverableHandler}
        allProductHandler={allProductHandler}
        setDeliverableActive={setDeliverableActive}
        setAllProActive={setAllProActive}
        deliverableActive={deliverableActive}
        allProActive={allProActive}
        type="category"
      />
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default withRouter(Category);
