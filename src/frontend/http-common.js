import axios from "axios";

export default axios.create({
  baseURL: "http://api-bhoomiweb.mykar.in/api",
  headers: {
    "Content-type": "application/json"
  }
});

// Server URL
// http://api-bhoomiweb.mykar.in/api
// Local URL
// http://localhost:3000/api
