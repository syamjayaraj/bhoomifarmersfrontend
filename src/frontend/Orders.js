import "./App.css";

import Footer from "./Components/Footer/Footer";
import Header from "./Components/Header/Header";
import ProductList from "./Components/ProductList/ProductList";
import Slider from "./Components/Slider/Slider";
import { useDispatch, useSelector } from "react-redux";

import { useEffect, useState } from "react";
import Cart from "./Components/Cart/Cart";
import { getBestSellerProductsFrontend } from "../store/actions";
import Orders from "./Components/Orders/Orders";
function App() {
  const dispatch = useDispatch();

  const userData = useSelector((state) => state.UserFrontend.userData);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  useEffect(() => {
    dispatch(
      getBestSellerProductsFrontend({
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_type: 0,
      })
    );
  }, []);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);

  return (
    <div>
      <Header />
      <Cart />

      <Orders />
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
