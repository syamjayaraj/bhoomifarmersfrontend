import Footer from "./Components/Footer/Footer";
import Header from "./Components/Header/Header";
import ProductList from "./Components/ProductList/ProductList";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import Cart from "./Components/Cart/Cart";
import { withRouter } from "react-router-dom";
import { getSearchProductsFrontend } from "../store/actions";

function Search(props) {
  const dispatch = useDispatch();
  const value = props.match.params.value;

  const userData = useSelector((state) => state.UserFrontend.userData);
  const [deliverableActive, setDeliverableActive] = useState(false);
  const [allProActive, setAllProActive] = useState(false);
  const orderWindow = useSelector((state) => state.OrderFrontend.orderWindow);

  useEffect(() => {
    if (userData.customer_id) {
      deliverableHandler();
    } else {
      allProductHandler();
    }
  }, [userData, value]);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);

  const deliverableHandler = () => {
    if (deliverableActive === false) {
      setDeliverableActive(true);
      setAllProActive(false);
    }
    dispatch(
      getSearchProductsFrontend({
        search_value: value,
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_type: 0,
      })
    );
  };

  const allProductHandler = () => {
    if (allProActive === false) {
      setDeliverableActive(false);
      setAllProActive(true);
    }

    dispatch(
      getSearchProductsFrontend({
        search_value: value,
        delivery_date:
          orderWindow &&
          orderWindow.delivery_date &&
          orderWindow.delivery_date[0] &&
          orderWindow.delivery_date[0].date,
        product_type: 1,
      })
    );
  };

  return (
    <div>
      <Header />
      <Cart />
      <ProductList
        title="Search Results"
        deliverableHandler={deliverableHandler}
        allProductHandler={allProductHandler}
        setDeliverableActive={setDeliverableActive}
        setAllProActive={setAllProActive}
        deliverableActive={deliverableActive}
        allProActive={allProActive}
        type="category"
      />
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default withRouter(Search);
