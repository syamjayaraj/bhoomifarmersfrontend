import "./App.css";

import Footer from "./Components/Footer/Footer";
import Header from "./Components/Header/Header";
import ProductList from "./Components/ProductList/ProductList";
import { useSelector } from "react-redux";

import Cart from "./Components/Cart/Cart";
import Details from "./Components/Details/Details";
import { withRouter } from "react-router";
import { useEffect, useState } from "react";

function ProductDetails() {
  let detailProduct = useSelector((state) => state.ProductFrontend.product);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, [detailProduct]);

  return (
    <div>
      <Header />
      <Cart />

      <Details key="lorem" />
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default withRouter(ProductDetails);
