import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import toastr from "toastr";
import jwt_decode from "jwt-decode";

import SweetAlert from "react-bootstrap-sweetalert";
import { MDBDataTable } from "mdbreact";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Label,
  Modal,
} from "reactstrap";

import Select from "react-select";
import { useDispatch, useSelector } from "react-redux";

import {
  getProducts,
  addProduct,
  deleteProduct,
  apiError,
  updateProduct,
  toggleProductActiveStatus,
  getCategoriesOptions,
  getUnitsOptions,
} from "../../store/actions";

// Redux
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb";
import "./product.scss";

const Products = (props) => {
  const [toggleSwitch, settoggleSwitch] = useState(true);
  const [productObject, setProductObject] = useState({});
  const [productsTemp, setProductsTemp] = useState([]);

  const [productIdTobeUpdated, setProductIdToBeUpdated] = useState(null);
  const [productIdToBeDeleted, setProductIdToBeDeleted] = useState(null);
  const [confirmDeleteAlert, setConfirmDeleteAlert] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [selectedUnit, setSelectedUnit] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [userId, setUserId] = useState(1);

  const {
    products,
    addingProduct,
    addProductResponse,
    deleteProductResponse,
    updateProductResponse,
    statusProductResponse,
    error,
  } = useSelector((state) => state.products);
  const categoryListOptions = useSelector(
    (state) => state.categories.categoryOptions
  );
  const unitListOptions = useSelector((state) => state.units.unitOptions);
  const dispatch = useDispatch();

  console.log(products, "po");

  useEffect(() => {
    dispatch(getProducts());
    dispatch(getCategoriesOptions());
    dispatch(getUnitsOptions());
  }, []);

  useEffect(() => {
    if (localStorage.getItem("authUser")) {
      const obj = jwt_decode(localStorage.getItem("authUser"));
      console.log(obj);

      setUserId(obj.user);
      setProductObject({ ["auth_userid"]: userId });
    }
  }, [props.success, props]);

  useEffect(() => {
    if (addProductResponse.type === "success") {
      toastr.success(addProductResponse.message);
    } else if (addProductResponse.type === "failure") {
      toastr.error(error.data.message, addProductResponse.message);
    }
  }, [addProductResponse]);

  useEffect(() => {
    if (deleteProductResponse.type === "success") {
      toastr.success(deleteProductResponse.message);
    } else if (deleteProductResponse.type === "failure") {
      toastr.error(error.data.message, deleteProductResponse.message);
    }
  }, [deleteProductResponse]);

  useEffect(() => {
    if (statusProductResponse.type === "success") {
      toastr.success(statusProductResponse.message);
    } else if (statusProductResponse.type === "failure") {
      toastr.error(error.data.message, statusProductResponse.message);
    }
  }, [statusProductResponse]);

  useEffect(() => {
    if (updateProductResponse.type === "success") {
      setShowModal(false);
      setProductIdToBeUpdated(null);
      // setPasswordObject({});
      setProductIdToBeUpdated(null);
      toastr.success(updateProductResponse.message);
    } else if (updateProductResponse.type === "failure") {
      toastr.error(error.data.message, updateProductResponse.message);
    }
  }, [updateProductResponse]);

  let preUpdateProduct = (item) => {
    if (item.unit) {
      let unitss = {
        label: item.unit.default_unit,
        value: item.product_unit_id,
      };
      handleSelectedUnits(unitss);
    }
    if (item.category) {
      let cats = {
        label: item.category.category_name,
        value: item.product_category,
      };
      handleSelectedCategories(cats);
    }
    setProductIdToBeUpdated(item.product_id);
    setProductObject(item);
  };

  useEffect(() => {
    let productsDuplicate = JSON.parse(
      JSON.stringify(products ? products : [])
    );
    let productData = [];
    productsDuplicate.map((item, index) => {
      var temp = item;
      item.fname = item.User.fname + " " + item.User.lname;
      item.auth_userid = userId;
      item.category_name = item.category.category_name;
      item.product_qty = item.product_qty + " " + item.unit.default_unit;
      item.action = (
        <div style={{ display: "flex", justifyContent: "center" }}>
          {/* //   <i
            className="uil-key-skeleton"
            style={{ fontSize: "1.3em", cursor: "pointer" }}
            onClick={() => {
              preUpdateUserPassword(item);
            }}
          ></i> */}
          <i
            className="uil-edit-alt"
            style={{
              fontSize: "1.3em",
              cursor: "pointer",
              marginLeft: "1rem",
              marginRight: "1rem",
            }}
            onClick={() => {
              preUpdateProduct(temp);
            }}
          ></i>
          <i
            className="uil-trash-alt"
            style={{ fontSize: "1.3em", cursor: "pointer" }}
            onClick={() => {
              setProductIdToBeDeleted(item.product_id);
              setConfirmDeleteAlert(true);
            }}
          ></i>
        </div>
      );
      item.id = index + 1;
      if (item.product_image_main != "") {
        item.product_image_main = (
          <div style={{ textAlign: "center" }}>
            <img
              src={`products-images/${item.product_image_main}`}
              height="50"
              width="50"
            />
          </div>
        );
      }

      if (item.display_status === 0) {
        item.display_status = (
          <div style={{ textAlign: "center", width: "100%", display: "flex" }}>
            <div
              class="form-check form-switch mb-3"
              style={{ textAlign: "center" }}
            >
              <input
                type="checkbox"
                class="form-check-input"
                id="customSwitch2"
                defaultChecked
                onClick={(e) => {
                  dispatch(toggleProductActiveStatus(item.product_id, userId));
                  settoggleSwitch(!toggleSwitch);
                }}
              />
            </div>
          </div>
        );
      } else {
        item.display_status = (
          <div style={{ textAlign: "center", width: "100%" }}>
            <div
              class="form-check form-switch mb-3"
              style={{ textAlign: "center" }}
            >
              <input
                type="checkbox"
                class="form-check-input"
                id="customSwitch2"
                onClick={(e) => {
                  dispatch(toggleProductActiveStatus(item.product_id, userId));
                  settoggleSwitch(!toggleSwitch);
                }}
              />
            </div>
          </div>
        );
      }

      productData.push(item);
    });
    setProductsTemp(productData);
  }, [products]);

  const data = {
    columns: [
      {
        label: "#",
        field: "id",
        sort: "asc",
        width: 10,
      },

      {
        label: "Name",
        field: "product_name_english",
        sort: "asc",
        width: 70,
      },
      {
        label: "SKU",
        field: "product_sku_autogen",
        sort: "asc",
        width: 70,
      },
      {
        label: "Category",
        field: "category_name",
        sort: "asc",
        width: 70,
      },
      {
        label: "Quantity",
        field: "product_qty",
        sort: "asc",
        width: 70,
      },
      {
        label: "Price",
        field: "product_sp",
        sort: "asc",
        width: 70,
      },
      {
        label: "Image",
        field: "product_image_main",
        sort: "asc",
        width: 70,
      },

      {
        label: "Display",
        field: "display_status",
        sort: "asc",
        width: 70,
      },
      {
        label: "Added by",
        field: "fname",
        sort: "asc",
        width: 100,
      },

      {
        label: "Action",
        field: "action",
        sort: "disabled",
        width: 100,
      },
    ],
    rows: productsTemp,
  };

  let categoryOptionsData =
    categoryListOptions &&
    categoryListOptions.map((item) => {
      return {
        label: item.category_name,
        value: item.category_id,
      };
    });

  const categoriesOptionsGroup = [
    {
      options: categoryOptionsData,
    },
  ];

  let unitOptionsData =
    unitListOptions &&
    unitListOptions.map((item) => {
      return {
        label: item.default_unit,
        value: item.unit_id,
      };
    });

  const unitsOptionsGroup = [
    {
      options: unitOptionsData,
    },
  ];

  let handleChangeInput = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setProductObject({
      ...productObject,
      [name]: value,
    });
  };

  function handleSelectedCategories(e) {
    console.log(e);
    let name = "product_category";
    let value = e.value;

    setSelectedCategory(e);
    setProductObject({
      ...productObject,
      [name]: value,
    });
  }

  function handleSelectedUnits(e) {
    let name = "product_unit_id";
    let value = e.value;

    setSelectedUnit(e);
    setProductObject({
      ...productObject,
      [name]: value,
    });
  }
  const handleValidSubmitProduct = (event, values) => {
    productIdTobeUpdated
      ? dispatch(updateProduct(productObject))
      : dispatch(addProduct(productObject));
  };

  //   let handleChangeImageUpload =(event) => {
  // setProductObject({...productObject, unitlogo:event.target.files[0]})
  //   }

  return (
    <React.Fragment>
      {confirmDeleteAlert ? (
        <SweetAlert
          title=""
          showCancel
          confirmButtonText="Delete"
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          onConfirm={() => {
            dispatch(deleteProduct(productIdToBeDeleted, userId));
            setConfirmDeleteAlert(false);
          }}
          onCancel={() => setConfirmDeleteAlert(false)}
        >
          Are you sure you want to delete it?
        </SweetAlert>
      ) : null}
      <div className="page-content">
        <div className="container-fluid">
          <Breadcrumbs title="Home" breadcrumbItem="Products" />
          <Row>
            <Col xl="12">
              <Card>
                <CardBody>
                  <AvForm
                    className="needs-validation"
                    onValidSubmit={(e, v) => {
                      handleValidSubmitProduct(e, v);
                    }}
                  >
                    <Row>
                      <Col md={3}>
                        <div className="mb-3">
                          <Label>Category</Label>
                          <AvField
                            name="createdBy"
                            value={userId}
                            type="hidden"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom01"
                            onChange={handleChangeInput}
                          />
                          <Select
                            name="product_category"
                            value={selectedCategory}
                            onChange={(value) => {
                              handleSelectedCategories(value);
                            }}
                            options={categoriesOptionsGroup}
                            classNamePrefix="select2-selection"
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom01">
                            Product Name
                          </Label>

                          <AvField
                            name="product_name_english"
                            value={productObject.product_name_english}
                            placeholder="Product Name"
                            type="text"
                            errorMessage="Enter Product Name"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom01"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom01">
                            Product Code
                          </Label>

                          <AvField
                            name="product_sku_autogen"
                            value={productObject.product_sku_autogen}
                            placeholder="Product Code"
                            type="text"
                            errorMessage="Enter Product Code"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom01"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom02">Cost</Label>
                          <AvField
                            name="product_cost"
                            value={productObject.product_sp}
                            placeholder="Cost"
                            type="text"
                            errorMessage="Enter Cost"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom02"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom02">
                            Listing Price
                          </Label>
                          <AvField
                            name="product_lp"
                            value={productObject.product_lp}
                            placeholder="Listing Price"
                            type="text"
                            errorMessage="Enter Listing Price"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom02"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom02">
                            Selling Price
                          </Label>
                          <AvField
                            name="product_sp"
                            value={productObject.product_sp}
                            placeholder="Selling Price"
                            type="text"
                            errorMessage="Enter Selling Price"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom02"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom02">Quantity</Label>
                          <AvField
                            name="product_qty"
                            value={productObject.product_qty}
                            placeholder="Quantity"
                            type="text"
                            errorMessage="Enter Quantity"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom02"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md={3}>
                        <div className="mb-3">
                          <Label>Unit</Label>

                          <Select
                            name="product_unit_id"
                            value={selectedUnit}
                            onChange={(value) => {
                              handleSelectedUnits(value);
                            }}
                            options={unitsOptionsGroup}
                            classNamePrefix="select2-selection"
                            validate={{ required: { value: true } }}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom02">
                            Max.Order Quantity
                          </Label>
                          <AvField
                            name="product_moq"
                            value={productObject.product_moq}
                            placeholder="Max.Order Quantity"
                            type="text"
                            errorMessage="Enter Max.Order Quantity"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom02"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>

                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom03">
                            Upload File
                          </Label>
                          <AvField
                            name="product_image_main"
                            type="file"
                            errorMessage="Select File"
                            className="form-control"
                            id="validationCustom03"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom03">
                            Product Description
                          </Label>
                          <AvField
                            name="product_description"
                            value={productObject.product_description}
                            placeholder="Product Description"
                            type="textarea"
                            errorMessage="Enter Product Description"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom03"
                            rows="1"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom03">Meta Title</Label>
                          <AvField
                            name="product_seo_title"
                            value={productObject.product_seo_title}
                            placeholder="Meta Title"
                            type="text"
                            errorMessage="Enter Meta Title"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom03"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom03">Meta Keys</Label>
                          <AvField
                            name="product_seo_keys"
                            value={productObject.product_seo_keys}
                            placeholder="Meta Keys"
                            type="textarea"
                            errorMessage="Enter Meta Keys"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom03"
                            onChange={handleChangeInput}
                            rows="1"
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mb-3">
                          <Label htmlFor="validationCustom03">
                            Meta Description
                          </Label>
                          <AvField
                            name="product_seo_desc"
                            value={productObject.product_seo_desc}
                            placeholder="Meta Description"
                            type="textarea"
                            errorMessage="Enter Meta Description"
                            className="form-control"
                            validate={{ required: { value: true } }}
                            id="validationCustom03"
                            rows="1"
                            onChange={handleChangeInput}
                          />
                        </div>
                      </Col>
                      <Col md="3">
                        <div className="mt-4">
                          {productIdTobeUpdated ? (
                            <Button
                              color="primary"
                              type="submit"
                              disabled={addingProduct ? true : false}
                            >
                              {addingProduct ? "Updating" : "Update"}
                            </Button>
                          ) : (
                            <Button
                              color="primary"
                              type="submit"
                              disabled={addingProduct ? true : false}
                            >
                              {addingProduct ? "Adding" : "Submit"}
                            </Button>
                          )}
                        </div>
                      </Col>
                    </Row>
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col className="col-12">
              <Card>
                <CardBody>
                  <MDBDataTable responsive bordered data={data} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  // const { error } = state.Users;
  // return { error };
};

export default withRouter(connect(mapStateToProps, { apiError })(Products));

Products.propTypes = {
  error: PropTypes.any,
  products: PropTypes.array,
};
