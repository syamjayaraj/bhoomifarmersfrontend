import axios from "axios";
import { post, del, get, put } from "./api_helper";
import * as url from "./url_helper";

// Gets the logged in user data from local session
const getLoggedInUser = () => {
  const user = localStorage.getItem("user");
  if (user) return JSON.parse(user);
  return null;
};

//is user is logged in
const isUserAuthenticated = () => {
  return getLoggedInUser() !== null;
};

// Login Method
const login = (data) => post(url.POST_LOGIN, data);

export const getUserProfile = () => get(url.GET_USER_PROFILE);

// get users
export const getUsers = () => get(url.GET_USERS);

// add user
export const addUser = (user) => post(url.ADD_USER, user);
export const getUser = (userId) => get(url.GET_USER, userId);
// export const updateUser = (user) => put(url.UPDATE_USER, user);
export const updateUser = (user) => put(url.UPDATE_USER + "/" + user.id, user);
export const deleteUser = (userId) => del(url.DELETE_USER, userId);

// add unit
export const addUnit = (unit) => post(url.ADD_UNIT, unit);
export const getUnits = () => get(url.GET_UNITS);
export const getUnit = (unitId) => get(url.GET_UNIT, unitId);
export const updateUnit = (unit) =>
  put(url.UPDATE_UNIT + "/" + unit.unit_id, unit);
export const deleteUnit = (unitId, auth_user) =>
  del(url.DELETE_UNIT + "/" + auth_user, unitId);
// add community
export const addCommunity = (community) => post(url.ADD_COMMUNITY, community);
export const getCommunites = () => get(url.GET_COMMUNITIES);
export const getCommunity = (communityId) =>
  get(url.GET_COMMUNITY, communityId);
export const updateCommunity = (community) =>
  put(url.UPDATE_COMMUNITY + "/" + community.community_id, community);
export const deleteCommunity = (communityId, auth_user) =>
  del(url.DELETE_COMMUNITY + "/" + auth_user, communityId);

// add category
export const addCategory = (category) => post(url.ADD_CATEGORY, category);
export const getCategories = () => get(url.GET_CATEGORIES);
export const getCategory = (categoryId) => get(url.GET_CATEGORY, categoryId);
export const updateCategory = (category) =>
  put(url.UPDATE_CATEGORY + "/" + category.category_id, category);
export const deleteCategory = (categoryId, auth_user) =>
  del(url.DELETE_CATEGORY + "/" + auth_user, categoryId);
export const toggleActiveStatus = (categoryId, auth_user) =>
  del(url.CHANGE_STATUS_CATEGORY + "/" + auth_user, categoryId);

// add product
export const addProduct = (product) => post(url.ADD_PRODUCT, product);
export const getProducts = () => get(url.GET_PRODUCTS);
export const getProduct = (productId) => get(url.GET_PRODUCT, productId);
export const updateProduct = (product) =>
  put(url.UPDATE_PRODUCT + "/" + product.product_id, product);
export const deleteProduct = (productId, auth_user) =>
  del(url.DELETE_PRODUCT + "/" + auth_user, productId);
export const toggleProductActiveStatus = (productId, auth_user) =>
  del(url.CHANGE_STATUS_PRODUCT + "/" + auth_user, productId);

// add communityrequest
export const addCommunityrequest = (communityrequest) =>
  post(url.ADD_COMMUNITYREQUEST, communityrequest);
export const getCommunityrequests = () => get(url.GET_COMMUNITYREQUESTS);
export const getCommunityrequest = (communityrequestId) =>
  get(url.GET_COMMUNITYREQUEST, communityrequestId);
export const updateCommunityrequest = (communityrequest) =>
  put(
    url.UPDATE_COMMUNITYREQUEST + "/" + communityrequest.request_id,
    communityrequest
  );
export const deleteCommunityrequest = (communityrequestId, auth_user) =>
  del(url.DELETE_COMMUNITYREQUEST + "/" + auth_user, communityrequestId);

// tax
export const addTax = (tax) => post(url.ADD_TAX, tax);
export const getTaxes = () => get(url.GET_TAXES);
export const getTax = (taxId) => get(url.GET_TAX, taxId);
export const updateTax = (tax) =>
  put(url.UPDATE_TAX + "/" + tax.tax_slab_id, tax);
export const deleteTax = (taxId, auth_user) =>
  del(url.DELETE_TAX + "/" + auth_user, taxId);

// add stock
export const addStock = (stock) => post(url.ADD_STOCK, stock);
export const getStocks = () => get(url.GET_STOCKS);
export const getStock = (stockId) => get(url.GET_STOCK, stockId);
export const updateStock = (stock) =>
  put(url.UPDATE_STOCK + "/" + stock.stock_id, stock);
export const deleteStock = (stockId, auth_user) =>
  del(url.DELETE_STOCK + "/" + auth_user, stockId);

//stocklogs
export const addStocklog = (stocklog) => post(url.ADD_STOCKLOG, stocklog);
export const getStocklogs = () => get(url.GET_STOCKLOGS);
export const getStocklog = (stocklogId) => get(url.GET_STOCKLOG, stocklogId);
export const updateStocklog = (stocklog) =>
  put(url.UPDATE_STOCKLOG + "/" + stocklog.stock_log_id, stocklog);
export const deleteStocklog = (stocklogId, auth_user) =>
  del(url.DELETE_STOCKLOG + "/" + auth_user, stocklogId);

// add order
export const addOrder = (order) => post(url.ADD_ORDER, order);
export const getOrders = () => get(url.GET_ORDERS);
export const getOrder = (orderId) => get(url.GET_ORDER, orderId);
export const updateOrder = (order) =>
  put(url.UPDATE_ORDER + "/" + order.order_id, order);
export const deleteOrder = (orderId, auth_user) =>
  del(url.DELETE_ORDER + "/" + auth_user, orderId);
export const deliverOrder = (o_id, auth_user) =>
  del(url.DELIVER_ORDER + "/" + auth_user, o_id);
export const outofdeliverOrder = (o_id, auth_user) =>
  del(url.OUTOF_DELIVER_ORDER + "/" + auth_user, o_id);

// customer
// customer
export const addCustomer = (customer) => post(url.ADD_CUSTOMER, customer);
export const getCustomers = () => get(url.GET_CUSTOMERS);
export const getCustomer = (customerId) => get(url.GET_CUSTOMER, customerId);
export const updateCustomer = (customer) =>
  put(url.UPDATE_CUSTOMER + "/" + customer.customer_id, customer);
export const deleteCustomer = (customerId, auth_user) =>
  del(url.DELETE_CUSTOMER + "/" + auth_user, customerId);
//community options
export const getCommunitiesOptions = () => get(url.GET_COMMUNITIES_OPTIONS);

//community Schedule

export const getCommunitySchedule = () => get(url.GET_COMMUNITY_SCHEDULE);
export const toggleCommunityActiveStatus = (community) =>
  put(url.CHANGE_STATUS_COMMUNITY + "/" + community.community_id, community);
// put(url.CHANGE_STATUS_COMMUNITY + '/' +  communityId);

// get privilages options
export const getPrivilagesOptions = () => get(url.GET_PRIVILAGES_OPTIONS);
//get state list
export const getStatesOptions = () => get(url.GET_STATES_OPTIONS);
//get categories list
export const getCategoriesOptions = () => get(url.GET_CATEGORIES_OPTIONS);
//get units list
export const getUnitsOptions = () => get(url.GET_UNITS_OPTIONS);

// get companies options
export const getCompaniesOptions = () => get(url.GET_COMPANIES_OPTIONS);
// get menu options
export const getMenusOptions = () => get(url.GET_Menu_OPTIONS);

// get Branches options
export const getBranchesOptions = (companyId) =>
  get(url.GET_BRANCHES_OPTIONS + "/" + companyId);

export { getLoggedInUser, isUserAuthenticated, login };
// deliverylist

export const getDeliverylists = () => get(url.GET_DELIVERYLISTS);
export const getDeliverylist = (deliverylistId) =>
  get(url.GET_DELIVERYLIST, deliverylistId);

// Frontend

//Cart
export const getCart = (customerId) => get(`${url.GET_CART}/${customerId}`);

export const addToCart = (cart) => post(url.ADD_TO_CART, cart);

export const updateCartQty = (cart) => post(url.UPDATE_CART_QTY, cart);

export const deleteFromCart = (cartId) =>
  get(`${url.DELETE_FROM_CART}/${cartId.cartId}`);

//Products
export const getProductsFrontend = (payload) =>
  get(url.GET_PRODUCTS_FRONTEND, payload);
export const getBestSellerProductsFrontend = (payload) =>
  post(url.GET_BEST_SELLER_PRODUCTS_FRONTEND, payload);
export const getProductFrontend = (payload) =>
  post(url.GET_PRODUCT_FRONTEND, payload);
export const getCategoryWiseProductFrontend = (payload) =>
  post(url.GET_CATEGORY_WISE_PRODUCT_FRONTEND, payload);

export const getSearchProductsFrontend = (payload) =>
  post(url.GET_SEARCH_PRODUCTS_FRONTEND, payload);

//Community
export const getCommunitiesFrontend = () => get(url.GET_COMMUNITIES_FRONTEND);

//Order
export const getOrderWindowFrontend = (payload) =>
  post(url.GET_ORDER_WINDOW, payload);

//Category
export const getCategoriesFrontend = () => get(url.GET_CATEGORIES_FRONTEND);
