//LOGIN
export const POST_LOGIN = "user/login";

//PROFILE
export const POST_EDIT_JWT_PROFILE = "/post-jwt-profile";
export const POST_EDIT_PROFILE = "/post-fake-profile";

//USERS
export const GET_USERS = "/user/lists";
export const ADD_USER = "/user";
export const GET_USER = "/user";
export const UPDATE_USER = "/user";
export const DELETE_USER = "/user";
// Menu Options

export const GET_Menu_OPTIONS = "/menu";

// Privilages options
export const GET_PRIVILAGES_OPTIONS = "/privilege";

export const GET_STATES_OPTIONS = "/state";

// Companies options
export const GET_COMPANIES_OPTIONS = "/company/list/options";

// Branches options
export const GET_BRANCHES_OPTIONS = "/branch/list/options";

export const GET_USER_PROFILE = "/user";
// community schedule

export const GET_COMMUNITY_SCHEDULE = "/community/schedule";
export const CHANGE_STATUS_COMMUNITY = "/community/schedule/change_status";

//unit
export const GET_UNITS = "/unit";
export const ADD_UNIT = "/unit";
export const GET_UNIT = "/unit";
export const UPDATE_UNIT = "/unit";
export const DELETE_UNIT = "/unit";
export const GET_UNITS_OPTIONS = "/unit/getOptions";

//category
export const GET_CATEGORIES = "/category";
export const ADD_CATEGORY = "/category";
export const GET_CATEGORY = "/category";
export const UPDATE_CATEGORY = "/category";
export const DELETE_CATEGORY = "/category";
export const CHANGE_STATUS_CATEGORY = "/category/change_status";
export const GET_CATEGORIES_OPTIONS = "/category/getOptions";
//product
export const GET_PRODUCTS = "/product";
export const ADD_PRODUCT = "/product";
export const GET_PRODUCT = "/product";
export const UPDATE_PRODUCT = "/product";
export const DELETE_PRODUCT = "/product";
export const CHANGE_STATUS_PRODUCT = "/product/change_status";

//community
export const GET_COMMUNITIES = "/community";
export const ADD_COMMUNITY = "/community";
export const GET_COMMUNITY = "/community";
export const UPDATE_COMMUNITY = "/community";
export const DELETE_COMMUNITY = "/community";
//communityrequest
export const GET_COMMUNITYREQUESTS = "/communityrequest";
export const ADD_COMMUNITYREQUEST = "/communityrequest";
export const GET_COMMUNITYREQUEST = "/communityrequest";
export const UPDATE_COMMUNITYREQUEST = "/communityrequest";
export const DELETE_COMMUNITYREQUEST = "/communityrequest";
//tax
export const GET_TAXES = "/tax";
export const ADD_TAX = "/tax";
export const GET_TAX = "/tax";
export const UPDATE_TAX = "/tax";
export const DELETE_TAX = "/tax";

//stock
export const GET_STOCKS = "/stock";
export const ADD_STOCK = "/stock";
export const GET_STOCK = "/stock";
export const UPDATE_STOCK = "/stock";
export const DELETE_STOCK = "/stock";

//stocklog
export const GET_STOCKLOGS = "/stocklog";
export const ADD_STOCKLOG = "/stocklog";
export const GET_STOCKLOG = "/stocklog";
export const UPDATE_STOCKLOG = "/stocklog";
export const DELETE_STOCKLOG = "/stocklog";

//order
export const GET_ORDERS = "/vieworders";
export const ADD_ORDER = "/vieworders";
export const GET_ORDER = "/vieworders";
export const UPDATE_ORDER = "/vieworders";
export const DELETE_ORDER = "/vieworders";
export const DELIVER_ORDER = "/vieworders/deliver_order";
export const OUTOF_DELIVER_ORDER = "/vieworders/outof_deliver_order";

// deliverylist
export const GET_DELIVERYLISTS = "/deliverylist";

export const GET_DELIVERYLIST = "/deliverylist";

//customer
export const GET_CUSTOMERS = "/customer";
export const ADD_CUSTOMER = "/customer";
export const GET_CUSTOMER = "/customer";
export const UPDATE_CUSTOMER = "/customer";
export const DELETE_CUSTOMER = "/customer";

//community options
export const GET_COMMUNITIES_OPTIONS = "/community/list";

//Frontend

//Cart
export const GET_CART = "/api/view_cart";
export const ADD_TO_CART = "/api/add_to_cart";
export const UPDATE_CART_QTY = "/api/update_cart";
export const DELETE_FROM_CART = "/api/delete_cart";

//Products
export const GET_PRODUCTS_FRONTEND = "/api/all_product_list";
export const GET_BEST_SELLER_PRODUCTS_FRONTEND = "api/best_seller_productList";

export const GET_PRODUCT_FRONTEND = "api/product_details";

export const GET_CATEGORY_WISE_PRODUCT_FRONTEND =
  "/api/category_wise_productList";

export const GET_SEARCH_PRODUCTS_FRONTEND = "/api/search";

//Community
export const GET_COMMUNITIES_FRONTEND = "/api/communityList";

//Order
export const GET_ORDER_WINDOW = "/api/orderWindow";

//Category
export const GET_CATEGORIES_FRONTEND = "api/categoryList";
