import { all, fork } from "redux-saga/effects";

//public
import AccountSaga from "./auth/register/saga";
import AuthSaga from "./auth/login/saga";
import ForgetSaga from "./auth/forgetpwd/saga";
import ProfileSaga from "./auth/profile/saga";
import LayoutSaga from "./layout/saga";
import userSaga from "./users/saga";
import privilagesSaga from "./privilages/saga";
import companiesSaga from "./companies/saga";
import branchesSaga from "./branches/saga";
import menuSaga from "./menu/saga";
import unitSaga from "./unit/saga";
import communitySaga from "./community/saga";
import categorySaga from "./category/saga";
import productSaga from "./product/saga";
import communityrequestSaga from "./communityRequest/saga";
import statesSaga from "./states/saga";
import taxSaga from "./tax/saga";
import stockSaga from "./stock/saga";
import stocklogSaga from "./stocklog/saga";
import Ordersaga from "./vieworder/saga";
import customersaga from "./customer/saga";
import communityScheduleSaga from "./communitySchedule/saga";
import deliverylistSaga from "./deliverylist/saga";
import communityOptionSaga from "./communityOption/saga";

import CartSaga from "./frontend/cart/saga";
import ProductsFrontendSaga from "./frontend/products/saga";
import CommunityFrontendSaga from "./frontend/community/saga";
import OrderFrontendSaga from "./frontend/order/saga";
import CategoryFrontendSaga from "./frontend/categories/saga";

export default function* rootSaga() {
  yield all([
    //public
    AccountSaga(),
    fork(AuthSaga),
    ProfileSaga(),
    ForgetSaga(),
    fork(LayoutSaga),
    fork(userSaga),
    fork(privilagesSaga),
    fork(companiesSaga),
    fork(branchesSaga),
    fork(menuSaga),
    fork(unitSaga),
    fork(communitySaga),
    fork(categorySaga),
    fork(productSaga),
    fork(communityrequestSaga),
    fork(statesSaga),
    fork(taxSaga),
    fork(stockSaga),
    fork(stocklogSaga),
    fork(Ordersaga),
    fork(customersaga),
    fork(communityScheduleSaga),
    fork(deliverylistSaga),
    fork(communityOptionSaga),
    fork(CartSaga),
    fork(ProductsFrontendSaga),
    fork(CommunityFrontendSaga),
    fork(OrderFrontendSaga),
    fork(CategoryFrontendSaga),
  ]);
}
