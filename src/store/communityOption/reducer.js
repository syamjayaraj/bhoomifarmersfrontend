import {
    GET_COMMUNITIES_OPTIONS_SUCCESS,
    GET_COMMUNITIES_OPTIONS_FAIL,
  } from './actionTypes';
  
  const INIT_STATE = {
    communitiesOptions: [],
  };
  
  const Communities = (state = INIT_STATE, action) => {
    switch (action.type) {
      case GET_COMMUNITIES_OPTIONS_SUCCESS:
        return {
          ...state,
          communitiesOptions: action.payload,
        };
  
      case GET_COMMUNITIES_OPTIONS_FAIL:
        return {
          ...state,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default Communities;