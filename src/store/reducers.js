import { combineReducers } from "redux";

// Front
import Layout from "./layout/reducer";

// Authentication
import Login from "./auth/login/reducer";
import Account from "./auth/register/reducer";
import ForgetPassword from "./auth/forgetpwd/reducer";
import Profile from "./auth/profile/reducer";

import Users from "./users/reducer";
import privilages from "./privilages/reducer";
import companies from "./companies/reducer";
import branches from "./branches/reducer";
import Menus from "./menu/reducer";
import units from "./unit/reducer";
import Communities from "./community/reducer";
import categories from "./category/reducer";
import products from "./product/reducer";
import communityrequests from "./communityRequest/reducer";
import states from "./states/reducer";
import taxes from "./tax/reducer";
import stocks from "./stock/reducer";
import stocklogs from "./stocklog/reducer";
import orders from "./vieworder/reducer";
import customers from "./customer/reducer";
import communityschedules from "./communitySchedule/reducer";
import communityOption from "./community/reducer";
// import categories from './category/reducer';
import deliverylist from "./deliverylist/reducer";

//Frontend
//Cart
import CartFrontend from "./frontend/cart/reducer";
//Products
import ProductFrontend from "./frontend/products/reducer";
//Communities
import CommunityFrontend from "./frontend/community/reducer";
//Communities
import UserFrontend from "./frontend/user/reducer";
//Order
import OrderFrontend from "./frontend/order/reducer";
//Category
import CategoryFrontend from "./frontend/categories/reducer";

//UI
import UiFrontend from "./frontend/ui/reducer";

const rootReducer = combineReducers({
  // public
  Layout,
  Login,
  Account,
  ForgetPassword,
  Profile,
  Users,
  Menus,
  privilages,
  companies,
  branches,
  units,
  Communities,
  categories,
  products,
  communityrequests,
  states,
  taxes,
  stocks,
  stocklogs,
  orders,
  customers,
  communityschedules,
  deliverylist,
  communityOption,
  CartFrontend,
  ProductFrontend,
  CommunityFrontend,
  UserFrontend,
  OrderFrontend,
  CategoryFrontend,
  UiFrontend,
});

export default rootReducer;
