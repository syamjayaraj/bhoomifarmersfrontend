import {
  SHOW_LOGIN_MODAL,
  HIDE_LOGIN_MODAL,
  SHOW_CART,
  HIDE_CART,
  SET_SHOW_COMMUNITY_MODAL_GLOBALLY,
  SET_SHOW_SHARE_MODAL,
  SHOW_ORDER_WINDOW_POPUP,
  HIDE_ORDER_WINDOW_POPUP,
} from "./actionTypes";

export const setShowLoginModal = () => {
  return {
    type: SHOW_LOGIN_MODAL,
    payload: {},
  };
};

export const setHideLoginModal = () => {
  return {
    type: HIDE_LOGIN_MODAL,
    payload: {},
  };
};

export const setShowOrderWindowPopup = () => {
  return {
    type: SHOW_ORDER_WINDOW_POPUP,
    payload: {},
  };
};

export const setHideOrderWindowPopup = () => {
  return {
    type: HIDE_ORDER_WINDOW_POPUP,
    payload: {},
  };
};

export const setShowCart = () => {
  return {
    type: SHOW_CART,
    payload: {},
  };
};

export const setHideCart = () => {
  return {
    type: HIDE_CART,
    payload: {},
  };
};
export const setShowShareModal = (payload) => {
  return {
    type: SET_SHOW_SHARE_MODAL,
    payload: payload,
  };
};

export const setShowCommunityModalGlobally = (payload) => {
  return {
    type: SET_SHOW_COMMUNITY_MODAL_GLOBALLY,
    payload: payload,
  };
};
