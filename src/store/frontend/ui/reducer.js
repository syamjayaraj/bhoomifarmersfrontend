import {
  SHOW_LOGIN_MODAL,
  HIDE_LOGIN_MODAL,
  SHOW_CART,
  HIDE_CART,
  SET_SHOW_COMMUNITY_MODAL_GLOBALLY,
  SET_SHOW_SHARE_MODAL,
  SHOW_ORDER_WINDOW_POPUP,
  HIDE_ORDER_WINDOW_POPUP,
} from "./actionTypes";

const initialState = {
  showLoginModal: false,
  showCart: false,
  showCommunityModalGlobally: true,
  showShareModal: false,
  showOrderWindowPopup: false,
};

const UiFrontend = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LOGIN_MODAL:
      state = { ...state, showLoginModal: true };
      break;
    case HIDE_LOGIN_MODAL:
      state = { ...state, showLoginModal: false };
      break;
    case SHOW_ORDER_WINDOW_POPUP:
      state = { ...state, showOrderWindowPopup: true };
      break;
    case HIDE_ORDER_WINDOW_POPUP:
      state = { ...state, showOrderWindowPopup: false };
      break;
    case SHOW_CART:
      state = { ...state, showCart: true };
      break;
    case HIDE_CART:
      state = { ...state, showCart: false };
      break;

    case SET_SHOW_COMMUNITY_MODAL_GLOBALLY:
      state = { ...state, showCommunityModalGlobally: action.payload };
      break;
    case SET_SHOW_SHARE_MODAL:
      console.log(action.payload, "aac");
      state = { ...state, showShareModal: action.payload };
      break;
    default:
      state = { ...state };
      break;
  }
  return state;
};

export default UiFrontend;
