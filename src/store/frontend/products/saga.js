import { takeEvery, takeLatest, put, call } from "redux-saga/effects";

import {
  GET_CATEGORY_WISE_PRODUCT_FRONTEND,
  GET_PRODUCTS_FRONTEND,
  GET_PRODUCT_FRONTEND,
  GET_BEST_SELLER_PRODUCTS_FRONTEND,
  GET_SEARCH_PRODUCTS_FRONTEND,
} from "./actionTypes";

import {
  getProductsFrontendSuccess,
  getProductsFrontendFail,
  getCategoryWiseProductsFrontendSuccess,
  getCategoryWiseProductsFrontendFail,
  getProductFrontendSuccess,
  getProductFrontendFail,
  getBestSellerProductsFrontendSuccess,
  getBestSellerProductsFrontendFail,
  getSearchProductsFrontendSuccess,
  getSearchProductsFrontendFail,
} from "./actions";

//Include Both Helper File with needed methods
import {
  getProductsFrontend,
  getProductFrontend,
  getCategoryWiseProductFrontend,
  getBestSellerProductsFrontend,
  getSearchProductsFrontend,
} from "../../../helpers/backend_helper";

function* onGetProductsFrontend({ payload: payload }) {
  try {
    console.log(payload, "prodpay");
    const response = yield call(getProductsFrontend, payload);
    yield put(getProductsFrontendSuccess(response));
  } catch (error) {
    yield put(getProductsFrontendFail(error.response));
  }
}

function* onGetProductFrontend({ payload: payload }) {
  try {
    const response = yield call(getProductFrontend, payload);
    yield put(getProductFrontendSuccess(response));
  } catch (error) {
    yield put(getProductFrontendFail(error.response));
  }
}

function* onGetCategoryWiseProductFrontend({ payload: payload }) {
  try {
    console.log("1");

    const response = yield call(getCategoryWiseProductFrontend, payload);
    console.log("2", response);
    yield put(getCategoryWiseProductsFrontendSuccess(response));
  } catch (error) {
    yield put(getCategoryWiseProductsFrontendFail(error.response));
  }
}

function* onGetBestSellerProductsFrontend({ payload: payload }) {
  try {
    console.log(payload, "paylo");
    const response = yield call(getBestSellerProductsFrontend, payload);
    yield put(getBestSellerProductsFrontendSuccess(response));
  } catch (error) {
    yield put(getBestSellerProductsFrontendFail(error.response));
  }
}

function* onGetSearchProductsFrontend({ payload: payload }) {
  try {
    console.log(payload, "paylo2");
    const response = yield call(getSearchProductsFrontend, payload);
    console.log(response, "res2");
    yield put(getSearchProductsFrontendSuccess(response));
  } catch (error) {
    yield put(getSearchProductsFrontendFail(error.response));
  }
}

function* productsFrontendSaga() {
  yield takeLatest(GET_PRODUCTS_FRONTEND, onGetProductsFrontend);
  yield takeLatest(GET_PRODUCT_FRONTEND, onGetProductFrontend);

  yield takeLatest(
    GET_CATEGORY_WISE_PRODUCT_FRONTEND,
    onGetCategoryWiseProductFrontend
  );
  yield takeLatest(
    GET_BEST_SELLER_PRODUCTS_FRONTEND,
    onGetBestSellerProductsFrontend
  );

  yield takeLatest(GET_SEARCH_PRODUCTS_FRONTEND, onGetSearchProductsFrontend);
}

export default productsFrontendSaga;
