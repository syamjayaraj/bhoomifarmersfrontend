import {
  GET_PRODUCTS_FRONTEND,
  GET_PRODUCTS_FRONTEND_FAIL,
  GET_PRODUCTS_FRONTEND_SUCCESS,
  GET_PRODUCT_FRONTEND,
  GET_PRODUCT_FRONTEND_FAIL,
  GET_PRODUCT_FRONTEND_SUCCESS,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND_SUCCESS,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND_FAIL,
  GET_BEST_SELLER_PRODUCTS_FRONTEND,
  GET_BEST_SELLER_PRODUCTS_FRONTEND_SUCCESS,
  GET_BEST_SELLER_PRODUCTS_FRONTEND_FAIL,
  GET_SEARCH_PRODUCTS_FRONTEND,
  GET_SEARCH_PRODUCTS_FRONTEND_SUCCESS,
  GET_SEARCH_PRODUCTS_FRONTEND_FAIL,
} from "./actionTypes";

const INIT_STATE = {
  gettingProducts: false,
  products: [],
  error: {},
  product: [{}],
  allProducts: [],
};

const ProductFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PRODUCTS_FRONTEND:
      return {
        ...state,
        gettingProducts: true,
      };

    case GET_PRODUCTS_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingProducts: false,
        allProducts: action.payload.data,
        error: {},
      };

    case GET_PRODUCTS_FRONTEND_FAIL:
      return {
        ...state,
        gettingProducts: false,
        error: action.payload,
      };

    case GET_PRODUCT_FRONTEND:
      console.log("red1");
      return {
        ...state,
        gettingProducts: true,
      };

    case GET_PRODUCT_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingProducts: false,
        product: action.payload.data,
        error: {},
      };

    case GET_PRODUCT_FRONTEND_FAIL:
      return {
        ...state,
        gettingProducts: false,
        error: action.payload,
      };

    case GET_CATEGORY_WISE_PRODUCT_FRONTEND:
      console.log("bho");
      return {
        ...state,
        gettingProducts: true,
      };

    case GET_CATEGORY_WISE_PRODUCT_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingProducts: false,
        products: action.payload.data,
        error: {},
      };

    case GET_CATEGORY_WISE_PRODUCT_FRONTEND_FAIL:
      return {
        ...state,
        gettingProducts: false,
        error: action.payload,
      };

    case GET_BEST_SELLER_PRODUCTS_FRONTEND:
      console.log("hello, nj", action.payload);
      return {
        ...state,
        gettingProducts: true,
      };

    case GET_BEST_SELLER_PRODUCTS_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingProducts: false,
        products: action.payload.data,
        error: {},
      };

    case GET_BEST_SELLER_PRODUCTS_FRONTEND_FAIL:
      return {
        ...state,
        gettingProducts: false,
        error: action.payload,
      };

    case GET_SEARCH_PRODUCTS_FRONTEND:
      console.log("hello, nj", action.payload);
      return {
        ...state,
        gettingProducts: true,
        products: [],
      };

    case GET_SEARCH_PRODUCTS_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingProducts: false,
        products: action.payload.data,
        error: {},
      };

    case GET_SEARCH_PRODUCTS_FRONTEND_FAIL:
      return {
        ...state,
        gettingProducts: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default ProductFrontend;
