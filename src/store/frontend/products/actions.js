import {
  GET_PRODUCTS_FRONTEND,
  GET_PRODUCTS_FRONTEND_FAIL,
  GET_PRODUCTS_FRONTEND_SUCCESS,
  GET_PRODUCT_FRONTEND,
  GET_PRODUCT_FRONTEND_FAIL,
  GET_PRODUCT_FRONTEND_SUCCESS,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND_SUCCESS,
  GET_CATEGORY_WISE_PRODUCT_FRONTEND_FAIL,
  GET_BEST_SELLER_PRODUCTS_FRONTEND,
  GET_BEST_SELLER_PRODUCTS_FRONTEND_SUCCESS,
  GET_BEST_SELLER_PRODUCTS_FRONTEND_FAIL,
  GET_SEARCH_PRODUCTS_FRONTEND,
  GET_SEARCH_PRODUCTS_FRONTEND_SUCCESS,
  GET_SEARCH_PRODUCTS_FRONTEND_FAIL,
} from "./actionTypes";

export const getProductsFrontend = (payload) => ({
  type: GET_PRODUCTS_FRONTEND,
  payload: payload,
});

export const getProductsFrontendSuccess = (products) => ({
  type: GET_PRODUCTS_FRONTEND_SUCCESS,
  payload: products,
});

export const getProductsFrontendFail = (customerId) => ({
  type: GET_PRODUCTS_FRONTEND_FAIL,
  payload: customerId,
});

export const getProductFrontend = (payload) => ({
  type: GET_PRODUCT_FRONTEND,
  payload: payload,
});

export const getProductFrontendSuccess = (product) => ({
  type: GET_PRODUCT_FRONTEND_SUCCESS,
  payload: product,
});

export const getProductFrontendFail = (customerId) => ({
  type: GET_PRODUCT_FRONTEND_FAIL,
  payload: customerId,
});

export const getCategoryWiseProductsFrontend = (payload) => ({
  type: GET_CATEGORY_WISE_PRODUCT_FRONTEND,
  payload: payload,
});
export const getCategoryWiseProductsFrontendSuccess = (payload) => ({
  type: GET_CATEGORY_WISE_PRODUCT_FRONTEND_SUCCESS,
  payload: payload,
});

export const getCategoryWiseProductsFrontendFail = (payload) => ({
  type: GET_CATEGORY_WISE_PRODUCT_FRONTEND_FAIL,
  payload: payload,
});

export const getBestSellerProductsFrontend = (payload) => ({
  type: GET_BEST_SELLER_PRODUCTS_FRONTEND,
  payload: payload,
});
export const getBestSellerProductsFrontendSuccess = (payload) => ({
  type: GET_BEST_SELLER_PRODUCTS_FRONTEND_SUCCESS,
  payload: payload,
});

export const getBestSellerProductsFrontendFail = (payload) => ({
  type: GET_BEST_SELLER_PRODUCTS_FRONTEND_FAIL,
  payload: payload,
});

export const getSearchProductsFrontend = (payload) => ({
  type: GET_SEARCH_PRODUCTS_FRONTEND,
  payload: payload,
});
export const getSearchProductsFrontendSuccess = (payload) => ({
  type: GET_SEARCH_PRODUCTS_FRONTEND_SUCCESS,
  payload: payload,
});

export const getSearchProductsFrontendFail = (payload) => ({
  type: GET_SEARCH_PRODUCTS_FRONTEND_FAIL,
  payload: payload,
});
