import {
  GET_COMMUNITIES_FRONTEND,
  GET_COMMUNITIES_FRONTEND_FAIL,
  GET_COMMUNITIES_FRONTEND_SUCCESS,
  SET_COMMUNITY_GLOBALLY,
} from "./actionTypes";

const INIT_STATE = {
  gettingCommunities: false,
  communities: [],
  communitySelected: {},
  error: {},
};

const CommunityFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_COMMUNITIES_FRONTEND:
      return {
        ...state,
        gettingCommunities: true,
      };

    case GET_COMMUNITIES_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingCommunities: false,
        communities: action.payload.data,
        error: {},
      };

    case GET_COMMUNITIES_FRONTEND_FAIL:
      return {
        ...state,
        gettingCommunities: false,
        error: action.payload,
      };

    case SET_COMMUNITY_GLOBALLY:
      console.log(action.payload, "comm");
      localStorage.setItem("communitySelected", JSON.stringify(action.payload));

      return {
        ...state,
        communitySelected: action.payload,
      };

    default:
      return state;
  }
};

export default CommunityFrontend;
