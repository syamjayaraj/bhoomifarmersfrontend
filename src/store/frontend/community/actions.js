import {
  GET_COMMUNITIES_FRONTEND,
  GET_COMMUNITIES_FRONTEND_FAIL,
  GET_COMMUNITIES_FRONTEND_SUCCESS,
  SET_COMMUNITY_GLOBALLY,
} from "./actionTypes";

export const getCommunitiesFrontend = () => ({
  type: GET_COMMUNITIES_FRONTEND,
});

export const setCommunityGlobally = (suggestion) => ({
  type: SET_COMMUNITY_GLOBALLY,
  payload: suggestion,
});

export const getCommunitiesFrontendSuccess = (communities) => ({
  type: GET_COMMUNITIES_FRONTEND_SUCCESS,
  payload: communities,
});

export const getCommunitiesFrontendFail = (customerId) => ({
  type: GET_COMMUNITIES_FRONTEND_FAIL,
  payload: customerId,
});
