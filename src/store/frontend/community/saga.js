import { takeLatest, put, call } from "redux-saga/effects";

import { GET_COMMUNITIES_FRONTEND } from "./actionTypes";

import {
  getCommunitiesFrontendSuccess,
  getCommunitiesFrontendFail,
} from "./actions";

//Include Both Helper File with needed methods
import { getCommunitiesFrontend } from "../../../helpers/backend_helper";

function* onGetCommunitiesFrontend() {
  try {
    const response = yield call(getCommunitiesFrontend);
    yield put(getCommunitiesFrontendSuccess(response));
  } catch (error) {
    yield put(getCommunitiesFrontendFail(error.response));
  }
}

function* CommunityFrontendSaga() {
  yield takeLatest(GET_COMMUNITIES_FRONTEND, onGetCommunitiesFrontend);
}

export default CommunityFrontendSaga;
