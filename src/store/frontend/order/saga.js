import { takeLatest, put, call } from "redux-saga/effects";

import { GET_ORDER_WINDOW } from "./actionTypes";

import { getOrderWindowSuccess, getOrderWindowFail } from "./actions";

//Include Both Helper File with needed methods
import { getOrderWindowFrontend } from "../../../helpers/backend_helper";

function* onGetOrderWindowFrontend({ payload: community_id }) {
  try {
    const response = yield call(getOrderWindowFrontend, { community_id });
    yield put(getOrderWindowSuccess(response));
    console.log(response, "res");
  } catch (error) {
    yield put(getOrderWindowFail(error.response));
  }
}

function* CommunityFrontendSaga() {
  yield takeLatest(GET_ORDER_WINDOW, onGetOrderWindowFrontend);
}

export default CommunityFrontendSaga;
