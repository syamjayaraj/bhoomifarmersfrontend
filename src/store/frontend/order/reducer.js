import { GET_ORDER_WINDOW, GET_ORDER_WINDOW_SUCCESS } from "./actionTypes";

const INIT_STATE = {
  orderWindow: {
    delivery_date: [],
  },

  error: {},
};

const OrderFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_ORDER_WINDOW:
      return {
        ...state,
      };

    case GET_ORDER_WINDOW_SUCCESS:
      console.log(action.payload, "pauu");
      return {
        ...state,
        orderWindow: action.payload,
      };

    default:
      return state;
  }
};

export default OrderFrontend;
