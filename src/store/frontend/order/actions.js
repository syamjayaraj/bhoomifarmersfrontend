import { GET_ORDER_WINDOW, GET_ORDER_WINDOW_SUCCESS } from "./actionTypes";

export const getOrderWindow = (communityId) => ({
  type: GET_ORDER_WINDOW,
  payload: communityId,
});

export const getOrderWindowSuccess = (payload) => ({
  type: GET_ORDER_WINDOW_SUCCESS,
  payload: payload,
});
export const getOrderWindowFail = (payload) => ({
  type: GET_ORDER_WINDOW_SUCCESS,
  payload: payload,
});
