import { SET_USER_DATA_FRONTEND } from "./actionTypes";

export const setUserDataFrontend = (user) => ({
  type: SET_USER_DATA_FRONTEND,
  payload: user,
});
