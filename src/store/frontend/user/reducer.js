import { SET_USER_DATA_FRONTEND } from "./actionTypes";

const INIT_STATE = {
  userData: {
    customer_mob: "",
    customer_name: "",
    delivery_date: "",
    jwt_token: "",
  },
  error: {},
};

const UserFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_USER_DATA_FRONTEND:
      console.log(action.payload, "payuser");
      localStorage.setItem("userData", JSON.stringify(action.payload));
      return {
        ...state,
        userData: action.payload,
      };

    default:
      return state;
  }
};

export default UserFrontend;
