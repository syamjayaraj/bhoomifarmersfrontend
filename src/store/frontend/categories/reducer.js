import {
  GET_CATEGORIES_FRONTEND,
  GET_CATEGORIES_FRONTEND_FAIL,
  GET_CATEGORIES_FRONTEND_SUCCESS,
} from "./actionTypes";

const INIT_STATE = {
  gettingCategories: false,
  categories: [],
  error: {},
};

const CategoryFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_CATEGORIES_FRONTEND:
      return {
        ...state,
        gettingCategories: true,
      };

    case GET_CATEGORIES_FRONTEND_SUCCESS:
      return {
        ...state,
        gettingCategories: false,
        categories: action.payload.data,
        error: {},
      };

    case GET_CATEGORIES_FRONTEND_FAIL:
      return {
        ...state,
        gettingCategories: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default CategoryFrontend;
