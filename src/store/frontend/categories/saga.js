import { takeEvery, put, call } from "redux-saga/effects";

import { GET_CATEGORIES_FRONTEND } from "./actionTypes";

import {
  getCategoriesFrontendSuccess,
  getCategoriesFrontendFail,
} from "./actions";

//Include Both Helper File with needed methods
import { getCategoriesFrontend } from "../../../helpers/backend_helper";

function* onGetCategoriesFrontendSaga() {
  try {
    const response = yield call(getCategoriesFrontend);
    console.log(response, "ress");
    yield put(getCategoriesFrontendSuccess(response));
  } catch (error) {
    yield put(getCategoriesFrontendFail(error.response));
  }
}

function* categoriesFrontendSaga() {
  yield takeEvery(GET_CATEGORIES_FRONTEND, onGetCategoriesFrontendSaga);
}

export default categoriesFrontendSaga;
