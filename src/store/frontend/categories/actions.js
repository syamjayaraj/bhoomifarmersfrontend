import {
  GET_CATEGORIES_FRONTEND,
  GET_CATEGORIES_FRONTEND_FAIL,
  GET_CATEGORIES_FRONTEND_SUCCESS,
} from "./actionTypes";

export const getCategoriesFrontend = () => ({
  type: GET_CATEGORIES_FRONTEND,
});

export const getCategoriesFrontendSuccess = (categories) => ({
  type: GET_CATEGORIES_FRONTEND_SUCCESS,
  payload: categories,
});

export const getCategoriesFrontendFail = (customerId) => ({
  type: GET_CATEGORIES_FRONTEND_FAIL,
  payload: customerId,
});
