import { takeLatest, put, call } from "redux-saga/effects";

import {
  ADD_TO_CART,
  DELETE_FROM_CART,
  UPDATE_CART_QTY,
  GET_CART,
} from "./actionTypes";

import {
  addToCartFail,
  addToCartSuccess,
  deleteFromCartSuccess,
  deleteFromCartFail,
  updateCartQtySuccess,
  updateCartQtyFail,
  getCartSuccess,
  getCartFail,
} from "./actions";

//Include Both Helper File with needed methods
import {
  getCart,
  addToCart,
  deleteFromCart,
  updateCartQty,
} from "../../../helpers/backend_helper";

function* onAddCart({ payload: cart }) {
  try {
    const response = yield call(addToCart, cart);
    yield put(addToCartSuccess(response));
  } catch (error) {
    yield put(addToCartFail(error.response));
  }
}

function* onGetCart({ payload: customerId }) {
  try {
    console.log("getting", customerId);
    const response = yield call(getCart, customerId);
    yield put(getCartSuccess(response));
  } catch (error) {
    yield put(getCartFail(error.response));
  }
}

function* onDeleteCart({ payload: cartId }) {
  try {
    const response = yield call(deleteFromCart, cartId);
    yield put(deleteFromCartSuccess(response));
  } catch (error) {
    yield put(deleteFromCartFail(error.response));
  }
}

function* onUpdateCartQty({ payload: payload }) {
  try {
    const response = yield call(updateCartQty, payload);
    yield put(updateCartQtySuccess(response));
  } catch (error) {
    yield put(updateCartQtyFail(error.response));
  }
}

function* CartSaga() {
  yield takeLatest(GET_CART, onGetCart);
  yield takeLatest(ADD_TO_CART, onAddCart);
  yield takeLatest(DELETE_FROM_CART, onDeleteCart);
  yield takeLatest(UPDATE_CART_QTY, onUpdateCartQty);
}

export default CartSaga;
