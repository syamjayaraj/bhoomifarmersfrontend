import {
  ADD_TO_CART,
  ADD_TO_CART_FAIL,
  ADD_TO_CART_SUCCESS,
  DELETE_FROM_CART,
  DELETE_FROM_CART_FAIL,
  DELETE_FROM_CART_SUCCESS,
  GET_CART,
  GET_CART_FAIL,
  GET_CART_SUCCESS,
  UPDATE_CART_QTY,
  UPDATE_CART_QTY_FAIL,
  UPDATE_CART_QTY_SUCCESS,
  CLEAR_CART,
} from "./actionTypes";

export const addToCart = (cart) => ({
  type: ADD_TO_CART,
  payload: cart,
});

export const addToCartSuccess = (cart) => ({
  type: ADD_TO_CART_SUCCESS,
  payload: cart,
});

export const addToCartFail = (cart) => ({
  type: ADD_TO_CART_FAIL,
  payload: cart,
});

export const deleteFromCart = (cartId) => ({
  type: DELETE_FROM_CART,
  payload: { cartId },
});

export const deleteFromCartSuccess = (cartId) => ({
  type: DELETE_FROM_CART_SUCCESS,
  payload: cartId,
});

export const deleteFromCartFail = (cartId) => ({
  type: DELETE_FROM_CART_FAIL,
  payload: cartId,
});

export const updateCartQty = (cart) => ({
  type: UPDATE_CART_QTY,
  payload: cart,
});

export const updateCartQtySuccess = (payload) => ({
  type: UPDATE_CART_QTY_SUCCESS,
  payload: payload,
});

export const updateCartQtyFail = (cartId) => ({
  type: UPDATE_CART_QTY_FAIL,
  payload: cartId,
});

export const getCart = (customerId) => ({
  type: GET_CART,
  payload: customerId,
});

export const getCartSuccess = (cart) => ({
  type: GET_CART_SUCCESS,
  payload: cart,
});

export const getCartFail = (error) => ({
  type: GET_CART_FAIL,
  payload: error,
});

export const clearCart = () => ({
  type: CLEAR_CART,
});
