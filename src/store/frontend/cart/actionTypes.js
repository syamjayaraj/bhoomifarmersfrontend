export const ADD_TO_CART = "ADD_TO_CART";
export const ADD_TO_CART_SUCCESS = "ADD_TO_CART_SUCCESS";
export const ADD_TO_CART_FAIL = "ADD_TO_CART";

export const DELETE_FROM_CART = "DELETE_FROM_CART";
export const DELETE_FROM_CART_SUCCESS = "DELETE_FROM_CART_SUCCESS";
export const DELETE_FROM_CART_FAIL = "DELETE_FROM_CART_FAIL";

export const GET_CART = "GET_CART";
export const GET_CART_SUCCESS = "GET_CART_SUCCESS";
export const GET_CART_FAIL = "GET_CART_FAIL";

export const UPDATE_CART_QTY = "UPDATE_CART_QTY";
export const UPDATE_CART_QTY_SUCCESS = "UPDATE_CART_QTY_SUCCESS";
export const UPDATE_CART_QTY_FAIL = "UPDATE_CART_QTY_FAIL";

export const CLEAR_CART = "CLEAR_CART";
