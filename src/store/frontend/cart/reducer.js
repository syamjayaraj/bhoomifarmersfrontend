import { set } from "lodash-es";
import {
  ADD_TO_CART,
  ADD_TO_CART_FAIL,
  ADD_TO_CART_SUCCESS,
  GET_CART,
  GET_CART_SUCCESS,
  GET_CART_FAIL,
  DELETE_FROM_CART,
  DELETE_FROM_CART_SUCCESS,
  DELETE_FROM_CART_FAIL,
  UPDATE_CART_QTY,
  UPDATE_CART_QTY_SUCCESS,
  UPDATE_CART_QTY_FAIL,
  CLEAR_CART,
} from "./actionTypes";

const INIT_STATE = {
  addingCart: false,
  addCartResponse: {},
  updating: true,
  cart: [],
  error: {},
  cartIdToBeDeleted: "",
};

const CartFrontend = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        addingCart: true,
      };

    case ADD_TO_CART_SUCCESS:
      let cartAfterAdded = [...state.cart, action.payload.data];

      // localStorage.setItem("cart", JSON.stringify(cartAfterAdded));

      return {
        ...state,
        addingCart: false,
        addCartResponse: {
          type: "success",
          message: "Cart added successfully",
        },
        cart: cartAfterAdded,
        error: {},
      };

    case ADD_TO_CART_FAIL:
      return {
        ...state,
        addingCart: false,
        addCartResponse: {
          type: "failure",
          message: "Adding cart failed",
        },
        error: action.payload,
      };

    case DELETE_FROM_CART:
      return {
        ...state,
        cartIdToBeDeleted: action.payload.cartId,
      };

    case DELETE_FROM_CART_SUCCESS:
      let newCart = state.cart.filter(
        ({ cart_id }) => cart_id !== state.cartIdToBeDeleted
      );
      // localStorage.setItem("cart", JSON.stringify(newCart));

      return {
        ...state,
        cart: newCart,
        cartIdToBeDeleted: "",
      };

    case DELETE_FROM_CART_FAIL:
      return {
        ...state,
        addingCart: true,
      };

    case GET_CART:
      console.log(action.payload, "reducar");
      return {
        ...state,
      };

    case GET_CART_SUCCESS:
      return {
        ...state,
        cart: action.payload.data ? action.payload.data : state.cart,
      };
    case GET_CART_FAIL:
      return {
        ...state,
        error: action.payload,
      };

    case UPDATE_CART_QTY:
      return {
        ...state,
        updating: true,
      };

    case UPDATE_CART_QTY_SUCCESS:
      let updatedCart = state.cart.map((item) => {
        if (item.cart_id === action.payload.data.cart_id) {
          return {
            ...item,
            ...action.payload.data,
          };
        } else {
          return item;
        }
      });
      // localStorage.setItem("cart", JSON.stringify(updatedCart));
      return {
        ...state,
        cart: updatedCart,
      };

    case UPDATE_CART_QTY_FAIL:
      return {
        ...state,
        error: action.payload,
      };

    case CLEAR_CART:
      return {
        ...state,
        cart: [],
      };

    default:
      return state;
  }
};

export default CartFrontend;
